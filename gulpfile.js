'use strict';
var gulp = require("gulp")
var watch = require("gulp-watch")
var babel = require("gulp-babel")
var newer = require("gulp-newer");

var sourcemaps = require("gulp-sourcemaps");

var shell= require("shelljs");

gulp.task("watch-example", function() {
  gulp.watch("example-src/**/*.js",["build-example"]);
})

gulp.task("build-example", function() {
  let dest = "dist";
  return gulp.src("example-src/**/*.js").
  pipe(newer(dest)).
  pipe(babel({
    presets: ["es2015", "stage-0"],
    plugins: ["transform-runtime"]
  })).
  pipe(gulp.dest(dest))
})

gulp.task("html",function(){
  var dest = __dirname;
  gulp.src("src/**/*.html").
  pipe(gulp.dest(dest));
})

gulp.task("watch", ["build"] ,function() {
  gulp.watch(["src/**/*.js","../README.md"],["build"]);
})

gulp.task("documentation",function(){
  shell.exec("npm run documentation");
})

gulp.task("build",["html"],function() {
  let dest =__dirname;
  return gulp.src("src/**/*.js").
  pipe(sourcemaps.init()).
  pipe(newer(dest)).
  pipe(babel({
    presets: ["es2015", "stage-0"],
    plugins: ["transform-runtime"]
  })).
  pipe(sourcemaps.write()).
  pipe(gulp.dest(dest))
})

gulp.task("build-all",["build","documentation"])
