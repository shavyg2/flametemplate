# Flametemplate

Flame Template is a Template Engine that removes complexity from Template Engines and make it more user friendly.


To make it the Template Engine more accessible features were limited to just the basics so you could get started.


To create a Renderer use the FlameManager Class. This allows you to saved what is the base dir that the templates will be searched.





## Installation
        npm install flame-template

## Usage
      //es6
      import FlameManager from "flame-template";

      //commonjs
      var FlameManager = require("flame-template").FlameManager;

      var f = new FlameManager({d:"./views"});

      //This can be used as promise
      f.render("mytemplate.html",{...}).then(function(result){
        res.send(result);
      });

      //This can be used async with babel compiler/es7
      var result = await f.render("mytemplate.html",{...})
      res.send(result);

## Templating

### If

        //This syntax is going to change to <$if $age>
        <$if age>
          This will only render if {age:value} was passed in.
        </>


### else if

        //Syntax will change <$if $age <10>
        <$if age < 10>
        <div>my age is $age</div>

        //Syntax will change <$else if $age > 10 />
        <$else if age > 10/> //Close bracket is needed

        <div> I am a big person of age $age</div>
        </> //Triggers the end of the if else statement


### else


        //Syntax will change <$if $age <10>
        <$if age < 10>
        <div>my age is $age</div>

        //Syntax will change <$else if $age > 10 />
        <$else/> //Close bracket is needed

        <div> I am a big person of age $age</div>
        </> //Triggers the end of the if else statement


### for

        //Syntax might change <ul $for $place of $places>
        <ul $for place of places>
          <li>$place</li>
        </ul>
### Expression

        //useful when need to check something
        <div class="$expression{ $name.length>1? 'many':'one' }" />


### Displaying text

displaying text is dead simple. Just call $ and the name of the variable. You can drill down as well.

        $person.name.last.
