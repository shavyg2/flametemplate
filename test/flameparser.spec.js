"use strict";

var _typeof2 = require("babel-runtime/helpers/typeof");

var _typeof3 = _interopRequireDefault(_typeof2);

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _dist = require("../dist");

var _dist2 = _interopRequireDefault(_dist);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var sinon = require("sinon");
var should = require("chai").should();
var path = require("path");

var log = require("debug")("flame:test");

describe("FlameParser", function () {
  var f = undefined;
  beforeEach(function () {
    f = new _dist2.default();
  });

  it("should be able to parse an element", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(done) {
      var result;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              try {
                result = f.parse("<div>Simple div</div>");

                result[0].isElement().should.equal(true);
                result[1].isString().should.equal(true);
                result[1].text.should.equal("Simple div");
                result[2].isElement().should.equal(true);
                done();
              } catch (e) {
                done(e);
              }

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));
    return function (_x) {
      return ref.apply(this, arguments);
    };
  })());

  it("should be able to parser an element with a class", function () {
    var result = f.parse("<div class=\"my-class\"></div>");
    result.length.should.equal(2, "Not all elements were parsed");
    result[0].text.should.equal('<div class="my-class">');
    result[1].text.should.equal('</div>');
  });

  it("should be able to parse a looped element", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(done) {
      var _result, r;

      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              try {
                _result = f.parse("<div $for name of names><div>$name</div></div>");

                _result[0].isElement().should.equal(true);
                _result[0].isLoop().should.equal(true);
                _result[0].getLoopElement().should.equal("<div >");
                _result[0].getLoopElementType().should.equal("div");
                r = _result[0].getLoop();

                done();
              } catch (e) {
                done(e);
              }

            case 1:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));
    return function (_x2) {
      return ref.apply(this, arguments);
    };
  })());

  it("should be able to parse an element with empty template elements", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(done) {
      var _result2, _r;

      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              try {
                _result2 = f.parse("<$if hungry>pizza<$else if !hungry/>no food</>");

                _result2[0].isIf().should.equal(true);
                _r = _result2[0].getIf();

                _result2[2].isElse().should.equal(true);
                _r = _result2[2].getElseIf();

                _result2[4].isEmptyElement().should.equal(true);
                done();
              } catch (e) {
                done(e);
              }

            case 1:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));
    return function (_x3) {
      return ref.apply(this, arguments);
    };
  })());

  it("should be able to parse an element with empty template elements", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(done) {
      var _result3;

      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.prev = 0;
              _context4.next = 3;
              return f.renderTemplate("<$if hungry>pizza<$else if !hungry/>no food</>");

            case 3:
              _result3 = _context4.sent;

              done();
              _context4.next = 10;
              break;

            case 7:
              _context4.prev = 7;
              _context4.t0 = _context4["catch"](0);

              done(_context4.t0);

            case 10:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, this, [[0, 7]]);
    }));
    return function (_x4) {
      return ref.apply(this, arguments);
    };
  })());

  it("should be able to parse an element with empty template elements", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(done) {
      var _result4;

      return _regenerator2.default.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.prev = 0;
              _context5.next = 3;
              return f.resolveTemplate("\n\n<div $if hungry>\n   <h1>my belly</h1>\n</div>\n<$if hungry>\npizza\n<$else if !hungry/>\nno food\n</>\n<ul $for name of names>\n<li $for letter of name>\n$letter\n</li>\n</ul>\n");

            case 3:
              _result4 = _context5.sent;

              _result4({
                hungry: true,
                names: ["jon", "matt"]
              }).should.equal("<div ><h1>my belly</h1></div>pizza<ul ><li >jon</li><li >matt</li></ul>");
              done();
              _context5.next = 11;
              break;

            case 8:
              _context5.prev = 8;
              _context5.t0 = _context5["catch"](0);

              done(_context5.t0);

            case 11:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, this, [[0, 8]]);
    }));
    return function (_x5) {
      return ref.apply(this, arguments);
    };
  })());

  it("should be able to parse a template file", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(done) {
      var _result5, template;

      return _regenerator2.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _context6.prev = 0;
              _context6.next = 3;
              return f.resolveTemplateFile(path.resolve(__dirname, "template.html"));

            case 3:
              _result5 = _context6.sent;
              template = _result5({
                hungry: true,
                names: ["jon", "matt"]
              });

              template.should.equal("<div ><h1>my belly</h1></div>pizza<ul ><li >jon</li><li >matt</li></ul>");
              done();
              _context6.next = 12;
              break;

            case 9:
              _context6.prev = 9;
              _context6.t0 = _context6["catch"](0);

              done(_context6.t0);

            case 12:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6, this, [[0, 9]]);
    }));
    return function (_x6) {
      return ref.apply(this, arguments);
    };
  })());

  it("should be able to detect include elements", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(done) {
      var e, _result6;

      return _regenerator2.default.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              e = undefined;

              try {
                f.dir = __dirname;
                log(f.dir);
                _result6 = f.parse("<div><$include './template.html' /></div>");

                _result6[1].isElement().should.equal(true);
                _result6[1].isInclude().should.equal(true);
              } catch (_e) {
                e = _e;
              } finally {
                done(e);
              }

            case 2:
            case "end":
              return _context7.stop();
          }
        }
      }, _callee7, this);
    }));
    return function (_x7) {
      return ref.apply(this, arguments);
    };
  })());

  it("should be able to detect include elements from string", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(done) {
      var e, _result7;

      return _regenerator2.default.wrap(function _callee8$(_context8) {
        while (1) {
          switch (_context8.prev = _context8.next) {
            case 0:
              e = undefined;
              _context8.prev = 1;

              f.dirname = __dirname;
              _context8.next = 5;
              return f.resolveTemplate("<div><$include './template.html' /></div>");

            case 5:
              _result7 = _context8.sent;
              _context8.next = 11;
              break;

            case 8:
              _context8.prev = 8;
              _context8.t0 = _context8["catch"](1);

              e = _context8.t0;

            case 11:
              _context8.prev = 11;

              done(e);
              return _context8.finish(11);

            case 14:
            case "end":
              return _context8.stop();
          }
        }
      }, _callee8, this, [[1, 8, 11, 14]]);
    }));
    return function (_x8) {
      return ref.apply(this, arguments);
    };
  })());

  it("should be able to detect include elements from file", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(done) {
      var e, _result8;

      return _regenerator2.default.wrap(function _callee9$(_context9) {
        while (1) {
          switch (_context9.prev = _context9.next) {
            case 0:
              e = undefined;
              _context9.prev = 1;
              _context9.next = 4;
              return f.renderTemplateFile(path.resolve(__dirname, 'home.html'));

            case 4:
              _result8 = _context9.sent;
              _context9.next = 10;
              break;

            case 7:
              _context9.prev = 7;
              _context9.t0 = _context9["catch"](1);

              e = _context9.t0;

            case 10:
              _context9.prev = 10;

              done(e);
              return _context9.finish(10);

            case 13:
            case "end":
              return _context9.stop();
          }
        }
      }, _callee9, this, [[1, 7, 10, 13]]);
    }));
    return function (_x9) {
      return ref.apply(this, arguments);
    };
  })());

  it("should be able to render include elements", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee10(done) {
      var e, _result9;

      return _regenerator2.default.wrap(function _callee10$(_context10) {
        while (1) {
          switch (_context10.prev = _context10.next) {
            case 0:
              e = undefined;
              _context10.prev = 1;
              _context10.next = 4;
              return f.resolveTemplateFile(path.resolve(__dirname, 'simpleInclude.html'));

            case 4:
              _result9 = _context10.sent;

              _result9({ text: "This is test" }).should.equal("<div><div>This is test</div></div>");
              _context10.next = 11;
              break;

            case 8:
              _context10.prev = 8;
              _context10.t0 = _context10["catch"](1);

              e = _context10.t0;

            case 11:
              _context10.prev = 11;

              done(e);
              return _context10.finish(11);

            case 14:
            case "end":
              return _context10.stop();
          }
        }
      }, _callee10, this, [[1, 8, 11, 14]]);
    }));
    return function (_x10) {
      return ref.apply(this, arguments);
    };
  })());

  it('should be able to find child element', (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee11(done) {
      var e, _result10;

      return _regenerator2.default.wrap(function _callee11$(_context11) {
        while (1) {
          switch (_context11.prev = _context11.next) {
            case 0:
              e = undefined;

              try {
                _result10 = f.parse("<div><$child/></div>");

                _result10[1].isChild().should.equal(true);
                _result10[1].getChild().should.equal('<$child/>');
              } catch (_e) {
                e = _e;
              } finally {
                done(e);
              }

            case 2:
            case "end":
              return _context11.stop();
          }
        }
      }, _callee11, this);
    }));
    return function (_x11) {
      return ref.apply(this, arguments);
    };
  })());

  it('should be able to find parent element', (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee12(done) {
      var e, _result11;

      return _regenerator2.default.wrap(function _callee12$(_context12) {
        while (1) {
          switch (_context12.prev = _context12.next) {
            case 0:
              e = undefined;

              try {
                _result11 = f.parse("<$parent './parent.html'/><div>my block</div>");

                _result11[0].isParent().should.equal(true);
                _result11[0].getParent().should.equal('./parent.html');
              } catch (_e) {
                e = _e;
              } finally {
                done(e);
              }

            case 2:
            case "end":
              return _context12.stop();
          }
        }
      }, _callee12, this);
    }));
    return function (_x12) {
      return ref.apply(this, arguments);
    };
  })());

  it('should be able to render a child template', (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee13(done) {
      var e, _result12;

      return _regenerator2.default.wrap(function _callee13$(_context13) {
        while (1) {
          switch (_context13.prev = _context13.next) {
            case 0:
              e = undefined;
              _context13.prev = 1;
              _context13.next = 4;
              return f.resolveTemplateFile(path.resolve(__dirname, 'child.html'));

            case 4:
              _result12 = _context13.sent;

              should.exist(_result12);
              (typeof _result12 === "undefined" ? "undefined" : (0, _typeof3.default)(_result12)).should.equal('function');

              _context13.next = 12;
              break;

            case 9:
              _context13.prev = 9;
              _context13.t0 = _context13["catch"](1);

              e = _context13.t0;

            case 12:
              _context13.prev = 12;

              done(e);
              return _context13.finish(12);

            case 15:
            case "end":
              return _context13.stop();
          }
        }
      }, _callee13, this, [[1, 9, 12, 15]]);
    }));
    return function (_x13) {
      return ref.apply(this, arguments);
    };
  })());
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlc3QvZmxhbWVwYXJzZXIuc3BlYy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUM3QixJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7QUFDdEMsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDOztBQUUzQixJQUFJLEdBQUcsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUE7O0FBRXhDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsWUFBVztBQUNqQyxNQUFJLENBQUMsWUFBQSxDQUFDO0FBQ04sWUFBVSxDQUFDLFlBQVc7QUFDcEIsS0FBQyxHQUFHLG9CQUFpQixDQUFDO0dBQ3ZCLENBQUMsQ0FBQTs7QUFHRixJQUFFLENBQUMsb0NBQW9DO3lFQUFFLGlCQUFlLElBQUk7VUFFcEQsTUFBTTs7Ozs7QUFEWixrQkFBSTtBQUNFLHNCQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsQ0FBQzs7QUFDN0Msc0JBQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3pDLHNCQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN4QyxzQkFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFBO0FBQ3pDLHNCQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN6QyxvQkFBSSxFQUFFLENBQUM7ZUFDUixDQUFDLE9BQU8sQ0FBQyxFQUFFO0FBQ1Ysb0JBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtlQUNSOzs7Ozs7OztLQUNGOzs7O09BQUMsQ0FBQTs7QUFFRixJQUFFLENBQUMsa0RBQWtELEVBQUUsWUFBVztBQUNoRSxRQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7QUFDdkQsVUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSw4QkFBOEIsQ0FBQyxDQUFDO0FBQzlELFVBQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO0FBQ3RELFVBQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztHQUN2QyxDQUFDLENBQUE7O0FBR0YsSUFBRSxDQUFDLDBDQUEwQzt5RUFBRSxrQkFBZSxJQUFJO1VBRTFELE9BQU0sRUFLTixDQUFDOzs7Ozs7QUFOUCxrQkFBSTtBQUNFLHVCQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxnREFBZ0QsQ0FBQzs7QUFDdEUsdUJBQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3pDLHVCQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN0Qyx1QkFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDbEQsdUJBQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDOUMsaUJBQUMsR0FBRyxPQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFOztBQUMzQixvQkFBSSxFQUFFLENBQUM7ZUFDUixDQUFDLE9BQU8sQ0FBQyxFQUFFO0FBQ1Ysb0JBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtlQUNSOzs7Ozs7OztLQUNGOzs7O09BQUMsQ0FBQTs7QUFJRixJQUFFLENBQUMsaUVBQWlFO3lFQUFFLGtCQUFlLElBQUk7VUFFakYsUUFBTSxFQUVOLEVBQUM7Ozs7OztBQUhQLGtCQUFJO0FBQ0Usd0JBQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLGdEQUFnRCxDQUFDOztBQUN0RSx3QkFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDaEMsa0JBQUMsR0FBRyxRQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFOztBQUV6Qix3QkFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDdEMsa0JBQUMsR0FBRyxRQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7O0FBRTFCLHdCQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUM5QyxvQkFBSSxFQUFFLENBQUM7ZUFDUixDQUFDLE9BQU8sQ0FBQyxFQUFFO0FBQ1Ysb0JBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtlQUNSOzs7Ozs7OztLQUNGOzs7O09BQUMsQ0FBQTs7QUFFRixJQUFFLENBQUMsaUVBQWlFO3lFQUFFLGtCQUFlLElBQUk7VUFFakYsUUFBTTs7Ozs7Ozs7cUJBQVMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxnREFBZ0QsQ0FBQzs7O0FBQWpGLHNCQUFNOztBQUNWLGtCQUFJLEVBQUUsQ0FBQzs7Ozs7Ozs7QUFFUCxrQkFBSSxjQUFHLENBQUE7Ozs7Ozs7O0tBRVY7Ozs7T0FBQyxDQUFBOztBQUVGLElBQUUsQ0FBQyxpRUFBaUU7eUVBQUUsa0JBQWUsSUFBSTtVQUVqRixRQUFNOzs7Ozs7OztxQkFBUyxDQUFDLENBQUMsZUFBZSwwTEFleEM7OztBQWZRLHNCQUFNOztBQWdCVixzQkFBTSxDQUFDO0FBQ0wsc0JBQU0sRUFBRSxJQUFJO0FBQ1oscUJBQUssRUFBRSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUM7ZUFDdkIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLDJFQUEyRSxDQUFDO0FBQzNGLGtCQUFJLEVBQUUsQ0FBQzs7Ozs7Ozs7QUFFUCxrQkFBSSxjQUFHLENBQUE7Ozs7Ozs7O0tBRVY7Ozs7T0FBQyxDQUFBOztBQUVGLElBQUUsQ0FBQyx5Q0FBeUM7eUVBQUUsa0JBQWUsSUFBSTtVQUV6RCxRQUFNLEVBRU4sUUFBUTs7Ozs7Ozs7cUJBRk8sQ0FBQyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDOzs7QUFBOUUsc0JBQU07QUFFTixzQkFBUSxHQUFHLFFBQU0sQ0FBQztBQUNwQixzQkFBTSxFQUFFLElBQUk7QUFDWixxQkFBSyxFQUFFLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQztlQUN2QixDQUFDOztBQUNGLHNCQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssMkVBQTJFLENBQUM7QUFDakcsa0JBQUksRUFBRSxDQUFDOzs7Ozs7OztBQUVQLGtCQUFJLGNBQUcsQ0FBQTs7Ozs7Ozs7S0FFVjs7OztPQUFDLENBQUE7O0FBR0YsSUFBRSxDQUFDLDJDQUEyQzt5RUFBRSxrQkFBZSxJQUFJO1VBQzdELENBQUMsRUFJQyxRQUFNOzs7Ozs7QUFKUixlQUFDOztBQUNMLGtCQUFJO0FBQ0YsaUJBQUMsQ0FBQyxHQUFHLEdBQUcsU0FBUyxDQUFDO0FBQ2xCLG1CQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ1Asd0JBQU0sR0FBRyxDQUFDLENBQUMsS0FBSyw2Q0FBNkM7O0FBQ2pFLHdCQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN6Qyx3QkFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7ZUFDMUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtBQUNYLGlCQUFDLEdBQUcsRUFBRSxDQUFDO2VBQ1IsU0FBUztBQUNSLG9CQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7ZUFDVDs7Ozs7Ozs7S0FDRjs7OztPQUFDLENBQUE7O0FBR0YsSUFBRSxDQUFDLHVEQUF1RDt5RUFBRSxrQkFBZSxJQUFJO1VBQ3pFLENBQUMsRUFHQyxRQUFNOzs7Ozs7QUFIUixlQUFDOzs7QUFFSCxlQUFDLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQzs7cUJBQ0gsQ0FBQyxDQUFDLGVBQWUsNkNBQTZDOzs7QUFBN0Usc0JBQU07Ozs7Ozs7O0FBRVYsZUFBQyxlQUFLLENBQUM7Ozs7O0FBRVAsa0JBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzs7Ozs7Ozs7O0tBRVg7Ozs7T0FBQyxDQUFBOztBQUVGLElBQUUsQ0FBQyxxREFBcUQ7eUVBQUUsa0JBQWUsSUFBSTtVQUN2RSxDQUFDLEVBRUMsUUFBTTs7Ozs7O0FBRlIsZUFBQzs7O3FCQUVnQixDQUFDLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUMsV0FBVyxDQUFDLENBQUM7OztBQUF4RSxzQkFBTTs7Ozs7Ozs7QUFFVixlQUFDLGVBQUssQ0FBQzs7Ozs7QUFFUCxrQkFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzs7Ozs7Ozs7S0FFWDs7OztPQUFDLENBQUE7O0FBRUYsSUFBRSxDQUFDLDJDQUEyQzt5RUFBRSxtQkFBZSxJQUFJO1VBQzdELENBQUMsRUFFQyxRQUFNOzs7Ozs7QUFGUixlQUFDOzs7cUJBRWdCLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBQyxvQkFBb0IsQ0FBQyxDQUFDOzs7QUFBbEYsc0JBQU07O0FBQ1Ysc0JBQU0sQ0FBQyxFQUFDLElBQUksRUFBQyxjQUFjLEVBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQzs7Ozs7Ozs7QUFFakYsZUFBQyxnQkFBSyxDQUFDOzs7OztBQUVQLGtCQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Ozs7Ozs7OztLQUVYOzs7O09BQUMsQ0FBQTs7QUFFRixJQUFFLENBQUMsc0NBQXNDO3lFQUFDLG1CQUFlLElBQUk7VUFDdkQsQ0FBQyxFQUVDLFNBQU07Ozs7OztBQUZSLGVBQUM7O0FBQ0wsa0JBQUk7QUFDRSx5QkFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsc0JBQXNCLENBQUM7O0FBQzVDLHlCQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN2Qyx5QkFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7ZUFDaEQsQ0FBQyxPQUFPLEVBQUUsRUFBRTtBQUNYLGlCQUFDLEdBQUMsRUFBRSxDQUFBO2VBQ0wsU0FBUztBQUNSLG9CQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7ZUFDUjs7Ozs7Ozs7S0FDRjs7OztPQUFDLENBQUE7O0FBRUYsSUFBRSxDQUFDLHVDQUF1Qzt5RUFBQyxtQkFBZSxJQUFJO1VBQ3hELENBQUMsRUFFQyxTQUFNOzs7Ozs7QUFGUixlQUFDOztBQUNMLGtCQUFJO0FBQ0UseUJBQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDOztBQUNyRSx5QkFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDeEMseUJBQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2VBQ3JELENBQUMsT0FBTyxFQUFFLEVBQUU7QUFDWCxpQkFBQyxHQUFDLEVBQUUsQ0FBQTtlQUNMLFNBQVM7QUFDUixvQkFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO2VBQ1I7Ozs7Ozs7O0tBQ0Y7Ozs7T0FBQyxDQUFBOztBQUlGLElBQUUsQ0FBQywyQ0FBMkM7eUVBQUMsbUJBQWUsSUFBSTtVQUM1RCxDQUFDLEVBRUMsU0FBTTs7Ozs7O0FBRlIsZUFBQzs7O3FCQUVnQixDQUFDLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUMsWUFBWSxDQUFDLENBQUM7OztBQUExRSx1QkFBTTs7QUFDVixvQkFBTSxDQUFDLEtBQUssQ0FBQyxTQUFNLENBQUMsQ0FBQztBQUNyQixzQkFBUSxTQUFNLHVEQUFOLFNBQU0sR0FBRSxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDOzs7Ozs7Ozs7QUFHekMsZUFBQyxnQkFBRyxDQUFBOzs7OztBQUVKLGtCQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7Ozs7Ozs7OztLQUVWOzs7O09BQUMsQ0FBQTtDQUdILENBQUMsQ0FBQSIsImZpbGUiOiJ0ZXN0L2ZsYW1lcGFyc2VyLnNwZWMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgc2lub24gPSByZXF1aXJlKFwic2lub25cIik7XHJcbnZhciBzaG91bGQgPSByZXF1aXJlKFwiY2hhaVwiKS5zaG91bGQoKTtcclxudmFyIHBhdGggPSByZXF1aXJlKFwicGF0aFwiKTtcclxuaW1wb3J0IEZsYW1lUGFyc2VyIGZyb20gXCIuLi9kaXN0XCI7XHJcbnZhciBsb2cgPSByZXF1aXJlKFwiZGVidWdcIikoXCJmbGFtZTp0ZXN0XCIpXHJcblxyXG5kZXNjcmliZShcIkZsYW1lUGFyc2VyXCIsIGZ1bmN0aW9uKCkge1xyXG4gIGxldCBmO1xyXG4gIGJlZm9yZUVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICBmID0gbmV3IEZsYW1lUGFyc2VyKCk7XHJcbiAgfSlcclxuXHJcblxyXG4gIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gcGFyc2UgYW4gZWxlbWVudFwiLCBhc3luYyBmdW5jdGlvbihkb25lKSB7XHJcbiAgICB0cnkge1xyXG4gICAgICBsZXQgcmVzdWx0ID0gZi5wYXJzZShcIjxkaXY+U2ltcGxlIGRpdjwvZGl2PlwiKTtcclxuICAgICAgcmVzdWx0WzBdLmlzRWxlbWVudCgpLnNob3VsZC5lcXVhbCh0cnVlKTtcclxuICAgICAgcmVzdWx0WzFdLmlzU3RyaW5nKCkuc2hvdWxkLmVxdWFsKHRydWUpO1xyXG4gICAgICByZXN1bHRbMV0udGV4dC5zaG91bGQuZXF1YWwoXCJTaW1wbGUgZGl2XCIpXHJcbiAgICAgIHJlc3VsdFsyXS5pc0VsZW1lbnQoKS5zaG91bGQuZXF1YWwodHJ1ZSk7XHJcbiAgICAgIGRvbmUoKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgZG9uZShlKVxyXG4gICAgfVxyXG4gIH0pXHJcblxyXG4gIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gcGFyc2VyIGFuIGVsZW1lbnQgd2l0aCBhIGNsYXNzXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgbGV0IHJlc3VsdCA9IGYucGFyc2UoXCI8ZGl2IGNsYXNzPVxcXCJteS1jbGFzc1xcXCI+PC9kaXY+XCIpO1xyXG4gICAgcmVzdWx0Lmxlbmd0aC5zaG91bGQuZXF1YWwoMiwgXCJOb3QgYWxsIGVsZW1lbnRzIHdlcmUgcGFyc2VkXCIpO1xyXG4gICAgcmVzdWx0WzBdLnRleHQuc2hvdWxkLmVxdWFsKCc8ZGl2IGNsYXNzPVwibXktY2xhc3NcIj4nKTtcclxuICAgIHJlc3VsdFsxXS50ZXh0LnNob3VsZC5lcXVhbCgnPC9kaXY+Jyk7XHJcbiAgfSlcclxuXHJcblxyXG4gIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gcGFyc2UgYSBsb29wZWQgZWxlbWVudFwiLCBhc3luYyBmdW5jdGlvbihkb25lKSB7XHJcbiAgICB0cnkge1xyXG4gICAgICBsZXQgcmVzdWx0ID0gZi5wYXJzZShcIjxkaXYgJGZvciBuYW1lIG9mIG5hbWVzPjxkaXY+JG5hbWU8L2Rpdj48L2Rpdj5cIik7XHJcbiAgICAgIHJlc3VsdFswXS5pc0VsZW1lbnQoKS5zaG91bGQuZXF1YWwodHJ1ZSk7XHJcbiAgICAgIHJlc3VsdFswXS5pc0xvb3AoKS5zaG91bGQuZXF1YWwodHJ1ZSk7XHJcbiAgICAgIHJlc3VsdFswXS5nZXRMb29wRWxlbWVudCgpLnNob3VsZC5lcXVhbChcIjxkaXYgPlwiKTtcclxuICAgICAgcmVzdWx0WzBdLmdldExvb3BFbGVtZW50VHlwZSgpLnNob3VsZC5lcXVhbChcImRpdlwiKVxyXG4gICAgICBsZXQgciA9IHJlc3VsdFswXS5nZXRMb29wKCk7XHJcbiAgICAgIGRvbmUoKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgZG9uZShlKVxyXG4gICAgfVxyXG4gIH0pXHJcblxyXG5cclxuXHJcbiAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBwYXJzZSBhbiBlbGVtZW50IHdpdGggZW1wdHkgdGVtcGxhdGUgZWxlbWVudHNcIiwgYXN5bmMgZnVuY3Rpb24oZG9uZSkge1xyXG4gICAgdHJ5IHtcclxuICAgICAgbGV0IHJlc3VsdCA9IGYucGFyc2UoXCI8JGlmIGh1bmdyeT5waXp6YTwkZWxzZSBpZiAhaHVuZ3J5Lz5ubyBmb29kPC8+XCIpO1xyXG4gICAgICByZXN1bHRbMF0uaXNJZigpLnNob3VsZC5lcXVhbCh0cnVlKTtcclxuICAgICAgbGV0IHIgPSByZXN1bHRbMF0uZ2V0SWYoKTtcclxuXHJcbiAgICAgIHJlc3VsdFsyXS5pc0Vsc2UoKS5zaG91bGQuZXF1YWwodHJ1ZSk7XHJcbiAgICAgIHIgPSByZXN1bHRbMl0uZ2V0RWxzZUlmKCk7XHJcblxyXG4gICAgICByZXN1bHRbNF0uaXNFbXB0eUVsZW1lbnQoKS5zaG91bGQuZXF1YWwodHJ1ZSk7XHJcbiAgICAgIGRvbmUoKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgZG9uZShlKVxyXG4gICAgfVxyXG4gIH0pXHJcblxyXG4gIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gcGFyc2UgYW4gZWxlbWVudCB3aXRoIGVtcHR5IHRlbXBsYXRlIGVsZW1lbnRzXCIsIGFzeW5jIGZ1bmN0aW9uKGRvbmUpIHtcclxuICAgIHRyeSB7XHJcbiAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBmLnJlbmRlclRlbXBsYXRlKFwiPCRpZiBodW5ncnk+cGl6emE8JGVsc2UgaWYgIWh1bmdyeS8+bm8gZm9vZDwvPlwiKTtcclxuICAgICAgZG9uZSgpO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICBkb25lKGUpXHJcbiAgICB9XHJcbiAgfSlcclxuXHJcbiAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBwYXJzZSBhbiBlbGVtZW50IHdpdGggZW1wdHkgdGVtcGxhdGUgZWxlbWVudHNcIiwgYXN5bmMgZnVuY3Rpb24oZG9uZSkge1xyXG4gICAgdHJ5IHtcclxuICAgICAgbGV0IHJlc3VsdCA9IGF3YWl0IGYucmVzb2x2ZVRlbXBsYXRlKGBcclxuXHJcbjxkaXYgJGlmIGh1bmdyeT5cclxuICAgPGgxPm15IGJlbGx5PC9oMT5cclxuPC9kaXY+XHJcbjwkaWYgaHVuZ3J5PlxyXG5waXp6YVxyXG48JGVsc2UgaWYgIWh1bmdyeS8+XHJcbm5vIGZvb2RcclxuPC8+XHJcbjx1bCAkZm9yIG5hbWUgb2YgbmFtZXM+XHJcbjxsaSAkZm9yIGxldHRlciBvZiBuYW1lPlxyXG4kbGV0dGVyXHJcbjwvbGk+XHJcbjwvdWw+XHJcbmApO1xyXG4gICAgICByZXN1bHQoe1xyXG4gICAgICAgIGh1bmdyeTogdHJ1ZSxcclxuICAgICAgICBuYW1lczogW1wiam9uXCIsIFwibWF0dFwiXVxyXG4gICAgICB9KS5zaG91bGQuZXF1YWwoYDxkaXYgPjxoMT5teSBiZWxseTwvaDE+PC9kaXY+cGl6emE8dWwgPjxsaSA+am9uPC9saT48bGkgPm1hdHQ8L2xpPjwvdWw+YCk7XHJcbiAgICAgIGRvbmUoKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgZG9uZShlKVxyXG4gICAgfVxyXG4gIH0pXHJcblxyXG4gIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gcGFyc2UgYSB0ZW1wbGF0ZSBmaWxlXCIsIGFzeW5jIGZ1bmN0aW9uKGRvbmUpIHtcclxuICAgIHRyeSB7XHJcbiAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBmLnJlc29sdmVUZW1wbGF0ZUZpbGUocGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgXCJ0ZW1wbGF0ZS5odG1sXCIpKTtcclxuXHJcbiAgICAgIHZhciB0ZW1wbGF0ZSA9IHJlc3VsdCh7XHJcbiAgICAgICAgaHVuZ3J5OiB0cnVlLFxyXG4gICAgICAgIG5hbWVzOiBbXCJqb25cIiwgXCJtYXR0XCJdXHJcbiAgICAgIH0pO1xyXG4gICAgICB0ZW1wbGF0ZS5zaG91bGQuZXF1YWwoYDxkaXYgPjxoMT5teSBiZWxseTwvaDE+PC9kaXY+cGl6emE8dWwgPjxsaSA+am9uPC9saT48bGkgPm1hdHQ8L2xpPjwvdWw+YCk7XHJcbiAgICAgIGRvbmUoKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgZG9uZShlKVxyXG4gICAgfVxyXG4gIH0pXHJcblxyXG5cclxuICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGRldGVjdCBpbmNsdWRlIGVsZW1lbnRzXCIsIGFzeW5jIGZ1bmN0aW9uKGRvbmUpIHtcclxuICAgIGxldCBlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgZi5kaXIgPSBfX2Rpcm5hbWU7XHJcbiAgICAgIGxvZyhmLmRpcik7XHJcbiAgICAgIGxldCByZXN1bHQgPSBmLnBhcnNlKGA8ZGl2PjwkaW5jbHVkZSAnLi90ZW1wbGF0ZS5odG1sJyAvPjwvZGl2PmApO1xyXG4gICAgICByZXN1bHRbMV0uaXNFbGVtZW50KCkuc2hvdWxkLmVxdWFsKHRydWUpO1xyXG4gICAgICByZXN1bHRbMV0uaXNJbmNsdWRlKCkuc2hvdWxkLmVxdWFsKHRydWUpO1xyXG4gICAgfSBjYXRjaCAoX2UpIHtcclxuICAgICAgZSA9IF9lO1xyXG4gICAgfSBmaW5hbGx5IHtcclxuICAgICAgZG9uZShlKTtcclxuICAgIH1cclxuICB9KVxyXG5cclxuXHJcbiAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBkZXRlY3QgaW5jbHVkZSBlbGVtZW50cyBmcm9tIHN0cmluZ1wiLCBhc3luYyBmdW5jdGlvbihkb25lKSB7XHJcbiAgICBsZXQgZTtcclxuICAgIHRyeSB7XHJcbiAgICAgIGYuZGlybmFtZSA9IF9fZGlybmFtZTtcclxuICAgICAgbGV0IHJlc3VsdCA9IGF3YWl0IGYucmVzb2x2ZVRlbXBsYXRlKGA8ZGl2PjwkaW5jbHVkZSAnLi90ZW1wbGF0ZS5odG1sJyAvPjwvZGl2PmApO1xyXG4gICAgfSBjYXRjaCAoX2UpIHtcclxuICAgICAgZSA9IF9lO1xyXG4gICAgfSBmaW5hbGx5IHtcclxuICAgICAgZG9uZShlKTtcclxuICAgIH1cclxuICB9KVxyXG5cclxuICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGRldGVjdCBpbmNsdWRlIGVsZW1lbnRzIGZyb20gZmlsZVwiLCBhc3luYyBmdW5jdGlvbihkb25lKSB7XHJcbiAgICBsZXQgZTtcclxuICAgIHRyeSB7XHJcbiAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBmLnJlbmRlclRlbXBsYXRlRmlsZShwYXRoLnJlc29sdmUoX19kaXJuYW1lLCdob21lLmh0bWwnKSk7XHJcbiAgICB9IGNhdGNoIChfZSkge1xyXG4gICAgICBlID0gX2U7XHJcbiAgICB9IGZpbmFsbHkge1xyXG4gICAgICBkb25lKGUpO1xyXG4gICAgfVxyXG4gIH0pXHJcblxyXG4gIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gcmVuZGVyIGluY2x1ZGUgZWxlbWVudHNcIiwgYXN5bmMgZnVuY3Rpb24oZG9uZSkge1xyXG4gICAgbGV0IGU7XHJcbiAgICB0cnkge1xyXG4gICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgZi5yZXNvbHZlVGVtcGxhdGVGaWxlKHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsJ3NpbXBsZUluY2x1ZGUuaHRtbCcpKTtcclxuICAgICAgcmVzdWx0KHt0ZXh0OlwiVGhpcyBpcyB0ZXN0XCJ9KS5zaG91bGQuZXF1YWwoXCI8ZGl2PjxkaXY+VGhpcyBpcyB0ZXN0PC9kaXY+PC9kaXY+XCIpO1xyXG4gICAgfSBjYXRjaCAoX2UpIHtcclxuICAgICAgZSA9IF9lO1xyXG4gICAgfSBmaW5hbGx5IHtcclxuICAgICAgZG9uZShlKTtcclxuICAgIH1cclxuICB9KVxyXG5cclxuICBpdCgnc2hvdWxkIGJlIGFibGUgdG8gZmluZCBjaGlsZCBlbGVtZW50Jyxhc3luYyBmdW5jdGlvbihkb25lKXtcclxuICAgIGxldCBlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgbGV0IHJlc3VsdCA9IGYucGFyc2UoXCI8ZGl2PjwkY2hpbGQvPjwvZGl2PlwiKTtcclxuICAgICAgcmVzdWx0WzFdLmlzQ2hpbGQoKS5zaG91bGQuZXF1YWwodHJ1ZSk7XHJcbiAgICAgIHJlc3VsdFsxXS5nZXRDaGlsZCgpLnNob3VsZC5lcXVhbCgnPCRjaGlsZC8+Jyk7XHJcbiAgICB9IGNhdGNoIChfZSkge1xyXG4gICAgICBlPV9lXHJcbiAgICB9IGZpbmFsbHkge1xyXG4gICAgICBkb25lKGUpXHJcbiAgICB9XHJcbiAgfSlcclxuXHJcbiAgaXQoJ3Nob3VsZCBiZSBhYmxlIHRvIGZpbmQgcGFyZW50IGVsZW1lbnQnLGFzeW5jIGZ1bmN0aW9uKGRvbmUpe1xyXG4gICAgbGV0IGU7XHJcbiAgICB0cnkge1xyXG4gICAgICBsZXQgcmVzdWx0ID0gZi5wYXJzZShcIjwkcGFyZW50ICcuL3BhcmVudC5odG1sJy8+PGRpdj5teSBibG9jazwvZGl2PlwiKTtcclxuICAgICAgcmVzdWx0WzBdLmlzUGFyZW50KCkuc2hvdWxkLmVxdWFsKHRydWUpO1xyXG4gICAgICByZXN1bHRbMF0uZ2V0UGFyZW50KCkuc2hvdWxkLmVxdWFsKCcuL3BhcmVudC5odG1sJyk7XHJcbiAgICB9IGNhdGNoIChfZSkge1xyXG4gICAgICBlPV9lXHJcbiAgICB9IGZpbmFsbHkge1xyXG4gICAgICBkb25lKGUpXHJcbiAgICB9XHJcbiAgfSlcclxuXHJcblxyXG5cclxuICBpdCgnc2hvdWxkIGJlIGFibGUgdG8gcmVuZGVyIGEgY2hpbGQgdGVtcGxhdGUnLGFzeW5jIGZ1bmN0aW9uKGRvbmUpe1xyXG4gICAgbGV0IGU7XHJcbiAgICB0cnkge1xyXG4gICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgZi5yZXNvbHZlVGVtcGxhdGVGaWxlKHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsJ2NoaWxkLmh0bWwnKSk7XHJcbiAgICAgIHNob3VsZC5leGlzdChyZXN1bHQpO1xyXG4gICAgICAodHlwZW9mIHJlc3VsdCkuc2hvdWxkLmVxdWFsKCdmdW5jdGlvbicpO1xyXG5cclxuICAgIH0gY2F0Y2ggKF9lKSB7XHJcbiAgICAgIGU9X2VcclxuICAgIH0gZmluYWxseSB7XHJcbiAgICAgIGRvbmUoZSlcclxuICAgIH1cclxuICB9KVxyXG5cclxuXHJcbn0pXHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
