"use strict";

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _flamemanager = require("../dist/flamemanager");

var _flamemanager2 = _interopRequireDefault(_flamemanager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe("FlameManager", function () {
  var f = undefined;
  before(function () {
    f = new _flamemanager2.default({ d: __dirname });
  });

  it("should be able to render home2.html", (function () {
    var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(done) {
      var result;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return f.render('home2.html');

            case 3:
              result = _context.sent;

              console.log(result);
              done();
              _context.next = 11;
              break;

            case 8:
              _context.prev = 8;
              _context.t0 = _context["catch"](0);

              done(_context.t0);

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this, [[0, 8]]);
    }));
    return function (_x) {
      return ref.apply(this, arguments);
    };
  })());
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlc3QvZmxhbWVtYW5hZ2VyLnNwZWMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLFFBQVEsQ0FBQyxjQUFjLEVBQUMsWUFBVTtBQUM5QixNQUFJLENBQUMsWUFBQSxDQUFDO0FBQ04sUUFBTSxDQUFDLFlBQVU7QUFDZixLQUFDLEdBQUcsMkJBQWlCLEVBQUMsQ0FBQyxFQUFDLFNBQVMsRUFBQyxDQUFDLENBQUM7R0FDckMsQ0FBQyxDQUFBOztBQUdGLElBQUUsQ0FBQyxxQ0FBcUM7eUVBQUMsaUJBQWUsSUFBSTtVQUVwRCxNQUFNOzs7Ozs7O3FCQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDOzs7QUFBckMsb0JBQU07O0FBQ1YscUJBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDcEIsa0JBQUksRUFBRSxDQUFBOzs7Ozs7OztBQUVOLGtCQUFJLGFBQUcsQ0FBQzs7Ozs7Ozs7S0FFWDs7OztPQUFDLENBQUE7Q0FFTCxDQUFDLENBQUEiLCJmaWxlIjoidGVzdC9mbGFtZW1hbmFnZXIuc3BlYy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBGbGFtZU1hbmFnZXIgZnJvbSBcIi4uL2Rpc3QvZmxhbWVtYW5hZ2VyXCI7XHJcblxyXG5kZXNjcmliZShcIkZsYW1lTWFuYWdlclwiLGZ1bmN0aW9uKCl7XHJcbiAgICBsZXQgZjtcclxuICAgIGJlZm9yZShmdW5jdGlvbigpe1xyXG4gICAgICBmID0gbmV3IEZsYW1lTWFuYWdlcih7ZDpfX2Rpcm5hbWV9KTtcclxuICAgIH0pXHJcblxyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gcmVuZGVyIGhvbWUyLmh0bWxcIixhc3luYyBmdW5jdGlvbihkb25lKXtcclxuICAgICAgdHJ5e1xyXG4gICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBmLnJlbmRlcignaG9tZTIuaHRtbCcpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHJlc3VsdCk7XHJcbiAgICAgICAgZG9uZSgpXHJcbiAgICAgIH1jYXRjaChlKXtcclxuICAgICAgICBkb25lKGUpO1xyXG4gICAgICB9XHJcbiAgICB9KVxyXG5cclxufSlcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
