"use strict";

var _chai = require("chai");

var _chai2 = _interopRequireDefault(_chai);

var _string = require("../dist/string");

var _string2 = _interopRequireDefault(_string);

var _element = require("../dist/element");

var _element2 = _interopRequireDefault(_element);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var should = _chai2.default.should();

describe("An Element", function () {

  describe("Class", function () {});

  describe("Methods", function () {
    var e = undefined;
    before(function () {
      e = new _element2.default("<div class='$person'>$person $detail</div>");
    });

    it("selfpush should create temp property", function () {
      should.not.exist(e.temp);
      e.selfpush();
      should.exist(e.temp);
    });

    it("code should swap the temp object", function () {
      var _1 = e.temp;
      e.code();
      var _2 = e.temp;
      _1.should.not.equal(_2);
    });

    it("code should be parsed", function () {
      e.temp[0].should.equal("<div class='\"+(root.person || '')+\"'>\"+(root.person || '')+\" \"+(root.detail || '')+\"</div>");
    });
  });
});