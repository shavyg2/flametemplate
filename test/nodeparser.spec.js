"use strict";

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getIterator2 = require("babel-runtime/core-js/get-iterator");

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _nodeparser = require("../dist/nodeparser");

var _nodeparser2 = _interopRequireDefault(_nodeparser);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _element = require("../dist/element");

var _element2 = _interopRequireDefault(_element);

var _dist = require("../dist");

var _dist2 = _interopRequireDefault(_dist);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var should = require("chai").should();

describe('NodeParser', function () {

  describe('element', function () {

    it("should be open", function () {
      var e = new _element2.default("<div>");
      e.isOpen().should.equal(true);
    });

    it("should be close", function () {
      var e = new _element2.default("</div>");
      e.isClose().should.equal(true);
    });

    it("should be openclose", function () {
      var e = new _element2.default("<link href='some.css' />");
      e.isOpenClose().should.equal(true);
    });
  });

  describe('Structure', function () {
    var parser = undefined;
    beforeEach(function () {
      parser = new _nodeparser2.default();
    });
    it('should be able to get unstructured layout of template', function () {
      var str = "<div></div>";
      var structure = parser.parse(str);
      structure.length.should.equal(2);
    });

    it('should be able to get structured layout of template', function () {
      var str = "<div></div>";
      var structure = parser.getStructure(str);
      structure.length.should.equal(2);
    });

    it('should be able to get structured layout of template', function () {
      var str = "<div><div>Inner Div</div></div>";
      var structure = parser.parse(str);
      structure.length.should.equal(5);
    });

    it('should be able to get structured layout of template', function () {
      var str = "<div><div>Inner Div</div></div>";
      var structure = parser.getStructure(str);

      structure.length.should.equal(2);
      structure[0].children.length.should.equal(2);
      structure[0].children[0].children[0].text.should.equal("Inner Div");
    });
  });

  describe("FlameParser2", function () {
    var f = undefined;
    beforeEach(function () {
      f = new _dist2.default();
    });

    it("should be able to parse if", function () {
      var e = new _element2.default('<$if !hungry/>');
      e.getIf()[1].should.equal("!hungry");
    });

    it("should be able to find an else if", function () {
      var e = new _element2.default('<$else if !hungry/>');
      e.isElseIf().should.equal(true);
    });

    it("should be able to find open tag", function () {
      var e = new _element2.default('<$if pizza>');
      e.isOpen().should.equal(true);
      e = new _element2.default('<li>');
      e.isOpen().should.equal(true);
    });

    it("should be able to find an if tag", function () {
      var e = new _element2.default('<$if pizza>');
      e.isIf().should.equal(true);
    });

    it("should be able to find close tag", function () {
      var e = new _element2.default('</>');
      e.isClose().should.equal(true);
      e = new _element2.default('</li>');
      e.isClose().should.equal(true);
    });

    it("should be able to render for loop", function () {
      var result = f.renderTemplate("\n        <ul $for name of names>\n        <$if pizza>\n          pizza\n        </>\n        <li>$name</li>\n        </ul>");
      var str = "";

      var concat = function concat(arr, str) {
        if (arr && arr.length > 0) {
          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = (0, _getIterator3.default)(arr), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var a = _step.value;

              if (a.temp && a.temp.length > 0) {
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                  for (var _iterator2 = (0, _getIterator3.default)(a.temp), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var b = _step2.value;

                    str += b;
                  }
                } catch (err) {
                  _didIteratorError2 = true;
                  _iteratorError2 = err;
                } finally {
                  try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                      _iterator2.return();
                    }
                  } finally {
                    if (_didIteratorError2) {
                      throw _iteratorError2;
                    }
                  }
                }
              }if (a.children && a.children.length > 0) {
                str = concat(a.children, str);
              }
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }
        }return str;
      };
      str = concat(result, "");
    });

    describe("File Parsing", function () {
      var f = undefined;
      beforeEach(function () {
        f = new _dist2.default();
      });

      it("should be able to parser simple include file", (function () {
        var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(done) {
          var _e, result;

          return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _e = undefined;
                  _context.prev = 1;
                  _context.next = 4;
                  return f.renderTemplateFile(_path2.default.resolve(__dirname, 'simple.html'));

                case 4:
                  result = _context.sent;
                  _context.next = 10;
                  break;

                case 7:
                  _context.prev = 7;
                  _context.t0 = _context["catch"](1);

                  _e = _context.t0;

                case 10:
                  _context.prev = 10;

                  done(_e);
                  return _context.finish(10);

                case 13:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this, [[1, 7, 10, 13]]);
        }));
        return function (_x) {
          return ref.apply(this, arguments);
        };
      })());

      it("should be able to parser simple include file", (function () {
        var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(done) {
          var _e, _result;

          return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _e = undefined;
                  _context2.prev = 1;
                  _context2.next = 4;
                  return f.renderTemplateFile(_path2.default.resolve(__dirname, 'simpleInclude.html'));

                case 4:
                  _result = _context2.sent;
                  _context2.next = 10;
                  break;

                case 7:
                  _context2.prev = 7;
                  _context2.t0 = _context2["catch"](1);

                  _e = _context2.t0;

                case 10:
                  _context2.prev = 10;

                  done(_e);
                  return _context2.finish(10);

                case 13:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this, [[1, 7, 10, 13]]);
        }));
        return function (_x2) {
          return ref.apply(this, arguments);
        };
      })());

      it("should be able to parser complex template file", (function () {
        var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(done) {
          var _e, _result2;

          return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  _e = undefined;
                  _context3.prev = 1;
                  _context3.next = 4;
                  return f.renderTemplateFile(_path2.default.resolve(__dirname, 'template.html'));

                case 4:
                  _result2 = _context3.sent;
                  _context3.next = 11;
                  break;

                case 7:
                  _context3.prev = 7;
                  _context3.t0 = _context3["catch"](1);

                  console.log(_context3.t0.stack);
                  _e = _context3.t0;

                case 11:
                  _context3.prev = 11;

                  done(_e);
                  return _context3.finish(11);

                case 14:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3, this, [[1, 7, 11, 14]]);
        }));
        return function (_x3) {
          return ref.apply(this, arguments);
        };
      })());

      var testStr = undefined;
      it("should be able to parse parent file", (function () {
        var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(done) {
          var _result3;

          return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
              switch (_context4.prev = _context4.next) {
                case 0:
                  _context4.prev = 0;
                  _context4.next = 3;
                  return f.resolveTemplateFile(_path2.default.resolve(__dirname, 'child.html'));

                case 3:
                  _result3 = _context4.sent;

                  testStr = _result3();
                  done();
                  _context4.next = 11;
                  break;

                case 8:
                  _context4.prev = 8;
                  _context4.t0 = _context4["catch"](0);

                  done(_context4.t0);

                case 11:
                case "end":
                  return _context4.stop();
              }
            }
          }, _callee4, this, [[0, 8]]);
        }));
        return function (_x4) {
          return ref.apply(this, arguments);
        };
      })());

      it("should be able to parse parent file with variables", (function () {
        var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(done) {
          var _result4;

          return _regenerator2.default.wrap(function _callee5$(_context5) {
            while (1) {
              switch (_context5.prev = _context5.next) {
                case 0:
                  _context5.prev = 0;
                  _context5.next = 3;
                  return f.resolveTemplateFile(_path2.default.resolve(__dirname, 'childparse.html'));

                case 3:
                  _result4 = _context5.sent;

                  _result4({
                    title: "My Title",
                    hello: "hello world"
                  }).should.equal(testStr);
                  done();
                  _context5.next = 11;
                  break;

                case 8:
                  _context5.prev = 8;
                  _context5.t0 = _context5["catch"](0);

                  done(_context5.t0);

                case 11:
                case "end":
                  return _context5.stop();
              }
            }
          }, _callee5, this, [[0, 8]]);
        }));
        return function (_x5) {
          return ref.apply(this, arguments);
        };
      })());

      it("should be able to parse for loop with variables", (function () {
        var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(done) {
          var _result5;

          return _regenerator2.default.wrap(function _callee6$(_context6) {
            while (1) {
              switch (_context6.prev = _context6.next) {
                case 0:
                  _context6.prev = 0;
                  _context6.next = 3;
                  return f.resolveTemplateFile(_path2.default.resolve(__dirname, 'forloop.html'));

                case 3:
                  _result5 = _context6.sent;

                  _result5({
                    title: "My Title",
                    menu: ["menu1", "menu2"]
                  });
                  done();
                  _context6.next = 11;
                  break;

                case 8:
                  _context6.prev = 8;
                  _context6.t0 = _context6["catch"](0);

                  done(_context6.t0);

                case 11:
                case "end":
                  return _context6.stop();
              }
            }
          }, _callee6, this, [[0, 8]]);
        }));
        return function (_x6) {
          return ref.apply(this, arguments);
        };
      })());

      it("should be able to parser home2.html", (function () {
        var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(done) {
          var _result6;

          return _regenerator2.default.wrap(function _callee7$(_context7) {
            while (1) {
              switch (_context7.prev = _context7.next) {
                case 0:
                  _context7.prev = 0;
                  _context7.next = 3;
                  return f.resolveTemplateFile(_path2.default.resolve(__dirname, 'home2.html'));

                case 3:
                  _result6 = _context7.sent;

                  debugger;
                  console.log(_result6.toString());
                  debugger;
                  console.log(_result6({ title: "FlameTemplate" }));
                  done();
                  _context7.next = 14;
                  break;

                case 11:
                  _context7.prev = 11;
                  _context7.t0 = _context7["catch"](0);

                  done(_context7.t0);

                case 14:
                case "end":
                  return _context7.stop();
              }
            }
          }, _callee7, this, [[0, 11]]);
        }));
        return function (_x7) {
          return ref.apply(this, arguments);
        };
      })());
    });
  });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlc3Qvbm9kZXBhcnNlci5zcGVjLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDOztBQUt0QyxRQUFRLENBQUMsWUFBWSxFQUFFLFlBQVc7O0FBRWhDLFVBQVEsQ0FBQyxTQUFTLEVBQUUsWUFBVzs7QUFFN0IsTUFBRSxtQkFBbUIsWUFBVztBQUM5QixVQUFJLENBQUMsR0FBRyxzQkFBWSxPQUFPLENBQUMsQ0FBQztBQUM3QixPQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUMvQixDQUFDLENBQUE7O0FBRUYsTUFBRSxvQkFBb0IsWUFBVztBQUMvQixVQUFJLENBQUMsR0FBRyxzQkFBWSxRQUFRLENBQUMsQ0FBQztBQUM5QixPQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNoQyxDQUFDLENBQUE7O0FBRUYsTUFBRSx3QkFBd0IsWUFBVztBQUNuQyxVQUFJLENBQUMsR0FBRyxzQkFBWSwwQkFBMEIsQ0FBQyxDQUFDO0FBQ2hELE9BQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3BDLENBQUMsQ0FBQTtHQUNILENBQUMsQ0FBQTs7QUFFRixVQUFRLENBQUMsV0FBVyxFQUFFLFlBQVc7QUFDL0IsUUFBSSxNQUFNLFlBQUEsQ0FBQztBQUNYLGNBQVUsQ0FBQyxZQUFXO0FBQ3BCLFlBQU0sR0FBRywwQkFBZ0IsQ0FBQztLQUMzQixDQUFDLENBQUE7QUFDRixNQUFFLENBQUMsdURBQXVELEVBQUUsWUFBVztBQUNyRSxVQUFJLEdBQUcsZ0JBQWdCLENBQUM7QUFDeEIsVUFBSSxTQUFTLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNsQyxlQUFTLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7S0FDbEMsQ0FBQyxDQUFBOztBQUVGLE1BQUUsQ0FBQyxxREFBcUQsRUFBRSxZQUFXO0FBQ25FLFVBQUksR0FBRyxnQkFBZ0IsQ0FBQztBQUN4QixVQUFJLFNBQVMsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3pDLGVBQVMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUNsQyxDQUFDLENBQUE7O0FBRUYsTUFBRSxDQUFDLHFEQUFxRCxFQUFFLFlBQVc7QUFDbkUsVUFBSSxHQUFHLG9DQUFvQyxDQUFDO0FBQzVDLFVBQUksU0FBUyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDbEMsZUFBUyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQ2xDLENBQUMsQ0FBQTs7QUFFRixNQUFFLENBQUMscURBQXFELEVBQUUsWUFBVztBQUNuRSxVQUFJLEdBQUcsb0NBQW9DLENBQUM7QUFDNUMsVUFBSSxTQUFTLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQzs7QUFFekMsZUFBUyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ2pDLGVBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDN0MsZUFBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7S0FDckUsQ0FBQyxDQUFBO0dBQ0gsQ0FBQyxDQUFBOztBQUdGLFVBQVEsQ0FBQyxjQUFjLEVBQUUsWUFBVztBQUNsQyxRQUFJLENBQUMsWUFBQSxDQUFDO0FBQ04sY0FBVSxDQUFDLFlBQVc7QUFDcEIsT0FBQyxHQUFHLG9CQUFrQixDQUFDO0tBQ3hCLENBQUMsQ0FBQTs7QUFFRixNQUFFLENBQUMsNEJBQTRCLEVBQUUsWUFBVztBQUMxQyxVQUFJLENBQUMsR0FBRyxzQkFBWSxnQkFBZ0IsQ0FBQyxDQUFDO0FBQ3RDLE9BQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0tBQ3RDLENBQUMsQ0FBQTs7QUFFRixNQUFFLENBQUMsbUNBQW1DLEVBQUUsWUFBVztBQUNqRCxVQUFJLENBQUMsR0FBRyxzQkFBWSxxQkFBcUIsQ0FBQyxDQUFDO0FBQzNDLE9BQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ2pDLENBQUMsQ0FBQTs7QUFHRixNQUFFLENBQUMsaUNBQWlDLEVBQUUsWUFBVztBQUMvQyxVQUFJLENBQUMsR0FBRyxzQkFBWSxhQUFhLENBQUMsQ0FBQztBQUNuQyxPQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUM5QixPQUFDLEdBQUcsc0JBQVksTUFBTSxDQUFDLENBQUE7QUFDdkIsT0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDL0IsQ0FBQyxDQUFBOztBQUVGLE1BQUUsQ0FBQyxrQ0FBa0MsRUFBRSxZQUFXO0FBQ2hELFVBQUksQ0FBQyxHQUFHLHNCQUFZLGFBQWEsQ0FBQyxDQUFDO0FBQ25DLE9BQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQzdCLENBQUMsQ0FBQTs7QUFFRixNQUFFLENBQUMsa0NBQWtDLEVBQUUsWUFBVztBQUNoRCxVQUFJLENBQUMsR0FBRyxzQkFBWSxLQUFLLENBQUMsQ0FBQztBQUMzQixPQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUMvQixPQUFDLEdBQUcsc0JBQVksT0FBTyxDQUFDLENBQUE7QUFDeEIsT0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDaEMsQ0FBQyxDQUFBOztBQUVGLE1BQUUsQ0FBQyxtQ0FBbUMsRUFBRSxZQUFXO0FBQ2pELFVBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxjQUFjLCtIQU1wQixDQUFBO0FBQ1QsVUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDOztBQUViLFVBQUksTUFBTSxHQUFHLFNBQVQsTUFBTSxDQUFZLEdBQUcsRUFBRSxHQUFHLEVBQUU7QUFDOUIsWUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDOzs7Ozs7QUFDdkIsNERBQWMsR0FBRyw0R0FBRTtrQkFBVixDQUFDOztBQUNSLGtCQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQzs7Ozs7O0FBQzdCLG1FQUFjLENBQUMsQ0FBQyxJQUFJLGlIQUFFO3dCQUFiLENBQUM7O0FBQ1IsdUJBQUcsSUFBSSxDQUFDLENBQUM7bUJBQ1Y7Ozs7Ozs7Ozs7Ozs7OztlQUFBLEFBQ0gsSUFBSSxDQUFDLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtBQUN2QyxtQkFBRyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2VBQy9CO2FBQ0Y7Ozs7Ozs7Ozs7Ozs7OztTQUFBLEFBQ0gsT0FBTyxHQUFHLENBQUM7T0FDWixDQUFBO0FBQ0QsU0FBRyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7S0FDMUIsQ0FBQyxDQUFBOztBQUVGLFlBQVEsQ0FBQyxjQUFjLEVBQUUsWUFBVztBQUNsQyxVQUFJLENBQUMsWUFBQSxDQUFDO0FBQ04sZ0JBQVUsQ0FBQyxZQUFXO0FBQ3BCLFNBQUMsR0FBRyxvQkFBa0IsQ0FBQTtPQUN2QixDQUFDLENBQUE7O0FBRUYsUUFBRSxDQUFDLDhDQUE4Qzs2RUFBRSxpQkFBZSxJQUFJO2NBQ2hFLEVBQUUsRUFFQSxNQUFNOzs7Ozs7QUFGUixvQkFBRTs7O3lCQUVlLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFLLE9BQU8sQ0FBQyxTQUFTLEVBQUUsYUFBYSxDQUFDLENBQUM7OztBQUEzRSx3QkFBTTs7Ozs7Ozs7QUFFVixvQkFBRSxjQUFJLENBQUM7Ozs7O0FBRVAsc0JBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQTs7Ozs7Ozs7O1NBRVg7Ozs7V0FBQyxDQUFBOztBQUVGLFFBQUUsQ0FBQyw4Q0FBOEM7NkVBQUUsa0JBQWUsSUFBSTtjQUNoRSxFQUFFLEVBRUEsT0FBTTs7Ozs7O0FBRlIsb0JBQUU7Ozt5QkFFZSxDQUFDLENBQUMsa0JBQWtCLENBQUMsZUFBSyxPQUFPLENBQUMsU0FBUyxFQUFFLG9CQUFvQixDQUFDLENBQUM7OztBQUFsRix5QkFBTTs7Ozs7Ozs7QUFFVixvQkFBRSxlQUFJLENBQUM7Ozs7O0FBRVAsc0JBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQTs7Ozs7Ozs7O1NBRVg7Ozs7V0FBQyxDQUFBOztBQUVGLFFBQUUsQ0FBQyxnREFBZ0Q7NkVBQUUsa0JBQWUsSUFBSTtjQUNsRSxFQUFFLEVBRUEsUUFBTTs7Ozs7O0FBRlIsb0JBQUU7Ozt5QkFFZSxDQUFDLENBQUMsa0JBQWtCLENBQUMsZUFBSyxPQUFPLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDOzs7QUFBN0UsMEJBQU07Ozs7Ozs7O0FBRVYseUJBQU8sQ0FBQyxHQUFHLENBQUMsYUFBRSxLQUFLLENBQUMsQ0FBQztBQUNyQixvQkFBRSxlQUFJLENBQUM7Ozs7O0FBRVAsc0JBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQTs7Ozs7Ozs7O1NBRVg7Ozs7V0FBQyxDQUFBOztBQUVGLFVBQUksT0FBTyxZQUFBLENBQUM7QUFDWixRQUFFLENBQUMscUNBQXFDOzZFQUFFLGtCQUFlLElBQUk7Y0FFckQsUUFBTTs7Ozs7Ozs7eUJBQVMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLGVBQUssT0FBTyxDQUFDLFNBQVMsRUFBRSxZQUFZLENBQUMsQ0FBQzs7O0FBQTNFLDBCQUFNOztBQUNWLHlCQUFPLEdBQUksUUFBTSxFQUFFLEFBQUMsQ0FBQztBQUNyQixzQkFBSSxFQUFFLENBQUM7Ozs7Ozs7O0FBRVAsc0JBQUksY0FBRyxDQUFBOzs7Ozs7OztTQUVWOzs7O1dBQUMsQ0FBQTs7QUFFRixRQUFFLENBQUMsb0RBQW9EOzZFQUFFLGtCQUFlLElBQUk7Y0FFcEUsUUFBTTs7Ozs7Ozs7eUJBQVMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLGVBQUssT0FBTyxDQUFDLFNBQVMsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDOzs7QUFBaEYsMEJBQU07O0FBQ1IsQUFBQywwQkFBTSxDQUFDO0FBQ04seUJBQUssRUFBRSxVQUFVO0FBQ2pCLHlCQUFLLEVBQUUsYUFBYTttQkFDckIsQ0FBQyxDQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDNUIsc0JBQUksRUFBRSxDQUFDOzs7Ozs7OztBQUVQLHNCQUFJLGNBQUcsQ0FBQTs7Ozs7Ozs7U0FFVjs7OztXQUFDLENBQUE7O0FBR0YsUUFBRSxDQUFDLGlEQUFpRDs2RUFBRSxrQkFBZSxJQUFJO2NBRWpFLFFBQU07Ozs7Ozs7O3lCQUFTLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxlQUFLLE9BQU8sQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDLENBQUM7OztBQUE3RSwwQkFBTTs7QUFDVixBQUFDLDBCQUFNLENBQUM7QUFDTix5QkFBSyxFQUFFLFVBQVU7QUFDakIsd0JBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUM7bUJBQ3pCLENBQUMsQ0FBRTtBQUNKLHNCQUFJLEVBQUUsQ0FBQzs7Ozs7Ozs7QUFFUCxzQkFBSSxjQUFHLENBQUE7Ozs7Ozs7O1NBRVY7Ozs7V0FBQyxDQUFBOztBQUVGLFFBQUUsQ0FBQyxxQ0FBcUM7NkVBQUMsa0JBQWUsSUFBSTtjQUVwRCxRQUFNOzs7Ozs7Ozt5QkFBUyxDQUFDLENBQUMsbUJBQW1CLENBQUMsZUFBSyxPQUFPLENBQUMsU0FBUyxFQUFDLFlBQVksQ0FBQyxDQUFDOzs7QUFBMUUsMEJBQU07O0FBQ1YsMkJBQVM7QUFDVCx5QkFBTyxDQUFDLEdBQUcsQ0FBQyxRQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztBQUMvQiwyQkFBUztBQUNULHlCQUFPLENBQUMsR0FBRyxDQUFDLFFBQU0sQ0FBQyxFQUFDLEtBQUssRUFBQyxlQUFlLEVBQUMsQ0FBQyxDQUFDLENBQUM7QUFDN0Msc0JBQUksRUFBRSxDQUFBOzs7Ozs7OztBQUVOLHNCQUFJLGNBQUcsQ0FBQzs7Ozs7Ozs7U0FFWDs7OztXQUFDLENBQUE7S0FHSCxDQUFDLENBQUE7R0FDSCxDQUFDLENBQUE7Q0FDSCxDQUFDLENBQUEiLCJmaWxlIjoidGVzdC9ub2RlcGFyc2VyLnNwZWMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgTm9kZVBhcnNlciBmcm9tICcuLi9kaXN0L25vZGVwYXJzZXInO1xyXG52YXIgc2hvdWxkID0gcmVxdWlyZShcImNoYWlcIikuc2hvdWxkKCk7XHJcbmltcG9ydCBwYXRoIGZyb20gXCJwYXRoXCI7XHJcbmltcG9ydCBlbGVtZW50IGZyb20gJy4uL2Rpc3QvZWxlbWVudCc7XHJcbmltcG9ydCBGbGFtZVBhcnNlcjIgZnJvbSBcIi4uL2Rpc3RcIjtcclxuXHJcbmRlc2NyaWJlKCdOb2RlUGFyc2VyJywgZnVuY3Rpb24oKSB7XHJcblxyXG4gIGRlc2NyaWJlKCdlbGVtZW50JywgZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgaXQoYHNob3VsZCBiZSBvcGVuYCwgZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBlID0gbmV3IGVsZW1lbnQoXCI8ZGl2PlwiKTtcclxuICAgICAgZS5pc09wZW4oKS5zaG91bGQuZXF1YWwodHJ1ZSk7XHJcbiAgICB9KVxyXG5cclxuICAgIGl0KGBzaG91bGQgYmUgY2xvc2VgLCBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIGUgPSBuZXcgZWxlbWVudChcIjwvZGl2PlwiKTtcclxuICAgICAgZS5pc0Nsb3NlKCkuc2hvdWxkLmVxdWFsKHRydWUpO1xyXG4gICAgfSlcclxuXHJcbiAgICBpdChgc2hvdWxkIGJlIG9wZW5jbG9zZWAsIGZ1bmN0aW9uKCkge1xyXG4gICAgICB2YXIgZSA9IG5ldyBlbGVtZW50KFwiPGxpbmsgaHJlZj0nc29tZS5jc3MnIC8+XCIpO1xyXG4gICAgICBlLmlzT3BlbkNsb3NlKCkuc2hvdWxkLmVxdWFsKHRydWUpO1xyXG4gICAgfSlcclxuICB9KVxyXG5cclxuICBkZXNjcmliZSgnU3RydWN0dXJlJywgZnVuY3Rpb24oKSB7XHJcbiAgICBsZXQgcGFyc2VyO1xyXG4gICAgYmVmb3JlRWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgcGFyc2VyID0gbmV3IE5vZGVQYXJzZXIoKTtcclxuICAgIH0pXHJcbiAgICBpdCgnc2hvdWxkIGJlIGFibGUgdG8gZ2V0IHVuc3RydWN0dXJlZCBsYXlvdXQgb2YgdGVtcGxhdGUnLCBmdW5jdGlvbigpIHtcclxuICAgICAgbGV0IHN0ciA9IGA8ZGl2PjwvZGl2PmA7XHJcbiAgICAgIHZhciBzdHJ1Y3R1cmUgPSBwYXJzZXIucGFyc2Uoc3RyKTtcclxuICAgICAgc3RydWN0dXJlLmxlbmd0aC5zaG91bGQuZXF1YWwoMik7XHJcbiAgICB9KVxyXG5cclxuICAgIGl0KCdzaG91bGQgYmUgYWJsZSB0byBnZXQgc3RydWN0dXJlZCBsYXlvdXQgb2YgdGVtcGxhdGUnLCBmdW5jdGlvbigpIHtcclxuICAgICAgbGV0IHN0ciA9IGA8ZGl2PjwvZGl2PmA7XHJcbiAgICAgIHZhciBzdHJ1Y3R1cmUgPSBwYXJzZXIuZ2V0U3RydWN0dXJlKHN0cik7XHJcbiAgICAgIHN0cnVjdHVyZS5sZW5ndGguc2hvdWxkLmVxdWFsKDIpO1xyXG4gICAgfSlcclxuXHJcbiAgICBpdCgnc2hvdWxkIGJlIGFibGUgdG8gZ2V0IHN0cnVjdHVyZWQgbGF5b3V0IG9mIHRlbXBsYXRlJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgIGxldCBzdHIgPSBgPGRpdj48ZGl2PklubmVyIERpdjwvZGl2PjwvZGl2PmA7XHJcbiAgICAgIHZhciBzdHJ1Y3R1cmUgPSBwYXJzZXIucGFyc2Uoc3RyKTtcclxuICAgICAgc3RydWN0dXJlLmxlbmd0aC5zaG91bGQuZXF1YWwoNSk7XHJcbiAgICB9KVxyXG5cclxuICAgIGl0KCdzaG91bGQgYmUgYWJsZSB0byBnZXQgc3RydWN0dXJlZCBsYXlvdXQgb2YgdGVtcGxhdGUnLCBmdW5jdGlvbigpIHtcclxuICAgICAgbGV0IHN0ciA9IGA8ZGl2PjxkaXY+SW5uZXIgRGl2PC9kaXY+PC9kaXY+YDtcclxuICAgICAgdmFyIHN0cnVjdHVyZSA9IHBhcnNlci5nZXRTdHJ1Y3R1cmUoc3RyKTtcclxuXHJcbiAgICAgIHN0cnVjdHVyZS5sZW5ndGguc2hvdWxkLmVxdWFsKDIpO1xyXG4gICAgICBzdHJ1Y3R1cmVbMF0uY2hpbGRyZW4ubGVuZ3RoLnNob3VsZC5lcXVhbCgyKTtcclxuICAgICAgc3RydWN0dXJlWzBdLmNoaWxkcmVuWzBdLmNoaWxkcmVuWzBdLnRleHQuc2hvdWxkLmVxdWFsKFwiSW5uZXIgRGl2XCIpO1xyXG4gICAgfSlcclxuICB9KVxyXG5cclxuXHJcbiAgZGVzY3JpYmUoXCJGbGFtZVBhcnNlcjJcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICBsZXQgZjtcclxuICAgIGJlZm9yZUVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgIGYgPSBuZXcgRmxhbWVQYXJzZXIyKCk7XHJcbiAgICB9KVxyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gcGFyc2UgaWZcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgIGxldCBlID0gbmV3IGVsZW1lbnQoJzwkaWYgIWh1bmdyeS8+Jyk7XHJcbiAgICAgIGUuZ2V0SWYoKVsxXS5zaG91bGQuZXF1YWwoXCIhaHVuZ3J5XCIpO1xyXG4gICAgfSlcclxuXHJcbiAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIGZpbmQgYW4gZWxzZSBpZlwiLCBmdW5jdGlvbigpIHtcclxuICAgICAgbGV0IGUgPSBuZXcgZWxlbWVudCgnPCRlbHNlIGlmICFodW5ncnkvPicpO1xyXG4gICAgICBlLmlzRWxzZUlmKCkuc2hvdWxkLmVxdWFsKHRydWUpO1xyXG4gICAgfSlcclxuXHJcblxyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBmaW5kIG9wZW4gdGFnXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICBsZXQgZSA9IG5ldyBlbGVtZW50KCc8JGlmIHBpenphPicpO1xyXG4gICAgICBlLmlzT3BlbigpLnNob3VsZC5lcXVhbCh0cnVlKTtcclxuICAgICAgZSA9IG5ldyBlbGVtZW50KCc8bGk+JylcclxuICAgICAgZS5pc09wZW4oKS5zaG91bGQuZXF1YWwodHJ1ZSk7XHJcbiAgICB9KVxyXG5cclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gZmluZCBhbiBpZiB0YWdcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgIGxldCBlID0gbmV3IGVsZW1lbnQoJzwkaWYgcGl6emE+Jyk7XHJcbiAgICAgIGUuaXNJZigpLnNob3VsZC5lcXVhbCh0cnVlKTtcclxuICAgIH0pXHJcblxyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBmaW5kIGNsb3NlIHRhZ1wiLCBmdW5jdGlvbigpIHtcclxuICAgICAgbGV0IGUgPSBuZXcgZWxlbWVudCgnPC8+Jyk7XHJcbiAgICAgIGUuaXNDbG9zZSgpLnNob3VsZC5lcXVhbCh0cnVlKTtcclxuICAgICAgZSA9IG5ldyBlbGVtZW50KCc8L2xpPicpXHJcbiAgICAgIGUuaXNDbG9zZSgpLnNob3VsZC5lcXVhbCh0cnVlKTtcclxuICAgIH0pXHJcblxyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byByZW5kZXIgZm9yIGxvb3BcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgIGxldCByZXN1bHQgPSBmLnJlbmRlclRlbXBsYXRlKGBcclxuICAgICAgICA8dWwgJGZvciBuYW1lIG9mIG5hbWVzPlxyXG4gICAgICAgIDwkaWYgcGl6emE+XHJcbiAgICAgICAgICBwaXp6YVxyXG4gICAgICAgIDwvPlxyXG4gICAgICAgIDxsaT4kbmFtZTwvbGk+XHJcbiAgICAgICAgPC91bD5gKVxyXG4gICAgICBsZXQgc3RyID0gXCJcIjtcclxuXHJcbiAgICAgIHZhciBjb25jYXQgPSBmdW5jdGlvbihhcnIsIHN0cikge1xyXG4gICAgICAgIGlmIChhcnIgJiYgYXJyLmxlbmd0aCA+IDApXHJcbiAgICAgICAgICBmb3IgKHZhciBhIG9mIGFycikge1xyXG4gICAgICAgICAgICBpZiAoYS50ZW1wICYmIGEudGVtcC5sZW5ndGggPiAwKVxyXG4gICAgICAgICAgICAgIGZvciAodmFyIGIgb2YgYS50ZW1wKSB7XHJcbiAgICAgICAgICAgICAgICBzdHIgKz0gYjtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChhLmNoaWxkcmVuICYmIGEuY2hpbGRyZW4ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgIHN0ciA9IGNvbmNhdChhLmNoaWxkcmVuLCBzdHIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHN0cjtcclxuICAgICAgfVxyXG4gICAgICBzdHIgPSBjb25jYXQocmVzdWx0LCBcIlwiKTtcclxuICAgIH0pXHJcblxyXG4gICAgZGVzY3JpYmUoXCJGaWxlIFBhcnNpbmdcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgIGxldCBmO1xyXG4gICAgICBiZWZvcmVFYWNoKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGYgPSBuZXcgRmxhbWVQYXJzZXIyKClcclxuICAgICAgfSlcclxuXHJcbiAgICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gcGFyc2VyIHNpbXBsZSBpbmNsdWRlIGZpbGVcIiwgYXN5bmMgZnVuY3Rpb24oZG9uZSkge1xyXG4gICAgICAgIGxldCBfZTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgbGV0IHJlc3VsdCA9IGF3YWl0IGYucmVuZGVyVGVtcGxhdGVGaWxlKHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsICdzaW1wbGUuaHRtbCcpKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICBfZSA9IGU7XHJcbiAgICAgICAgfSBmaW5hbGx5IHtcclxuICAgICAgICAgIGRvbmUoX2UpXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG5cclxuICAgICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBwYXJzZXIgc2ltcGxlIGluY2x1ZGUgZmlsZVwiLCBhc3luYyBmdW5jdGlvbihkb25lKSB7XHJcbiAgICAgICAgbGV0IF9lO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgZi5yZW5kZXJUZW1wbGF0ZUZpbGUocGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJ3NpbXBsZUluY2x1ZGUuaHRtbCcpKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICBfZSA9IGU7XHJcbiAgICAgICAgfSBmaW5hbGx5IHtcclxuICAgICAgICAgIGRvbmUoX2UpXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG5cclxuICAgICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBwYXJzZXIgY29tcGxleCB0ZW1wbGF0ZSBmaWxlXCIsIGFzeW5jIGZ1bmN0aW9uKGRvbmUpIHtcclxuICAgICAgICBsZXQgX2U7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBmLnJlbmRlclRlbXBsYXRlRmlsZShwYXRoLnJlc29sdmUoX19kaXJuYW1lLCAndGVtcGxhdGUuaHRtbCcpKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhlLnN0YWNrKTtcclxuICAgICAgICAgIF9lID0gZTtcclxuICAgICAgICB9IGZpbmFsbHkge1xyXG4gICAgICAgICAgZG9uZShfZSlcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcblxyXG4gICAgICBsZXQgdGVzdFN0cjtcclxuICAgICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBwYXJzZSBwYXJlbnQgZmlsZVwiLCBhc3luYyBmdW5jdGlvbihkb25lKSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBmLnJlc29sdmVUZW1wbGF0ZUZpbGUocGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJ2NoaWxkLmh0bWwnKSk7XHJcbiAgICAgICAgICB0ZXN0U3RyID0gKHJlc3VsdCgpKTtcclxuICAgICAgICAgIGRvbmUoKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICBkb25lKGUpXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG5cclxuICAgICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBwYXJzZSBwYXJlbnQgZmlsZSB3aXRoIHZhcmlhYmxlc1wiLCBhc3luYyBmdW5jdGlvbihkb25lKSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBmLnJlc29sdmVUZW1wbGF0ZUZpbGUocGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJ2NoaWxkcGFyc2UuaHRtbCcpKTtcclxuICAgICAgICAgICAgKHJlc3VsdCh7XHJcbiAgICAgICAgICAgICAgdGl0bGU6IFwiTXkgVGl0bGVcIixcclxuICAgICAgICAgICAgICBoZWxsbzogXCJoZWxsbyB3b3JsZFwiXHJcbiAgICAgICAgICAgIH0pKS5zaG91bGQuZXF1YWwodGVzdFN0cik7XHJcbiAgICAgICAgICBkb25lKCk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgZG9uZShlKVxyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuXHJcblxyXG4gICAgICBpdChcInNob3VsZCBiZSBhYmxlIHRvIHBhcnNlIGZvciBsb29wIHdpdGggdmFyaWFibGVzXCIsIGFzeW5jIGZ1bmN0aW9uKGRvbmUpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgbGV0IHJlc3VsdCA9IGF3YWl0IGYucmVzb2x2ZVRlbXBsYXRlRmlsZShwYXRoLnJlc29sdmUoX19kaXJuYW1lLCAnZm9ybG9vcC5odG1sJykpO1xyXG4gICAgICAgICAgKHJlc3VsdCh7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIk15IFRpdGxlXCIsXHJcbiAgICAgICAgICAgIG1lbnU6IFtcIm1lbnUxXCIsIFwibWVudTJcIl1cclxuICAgICAgICAgIH0pKTtcclxuICAgICAgICAgIGRvbmUoKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICBkb25lKGUpXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG5cclxuICAgICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBwYXJzZXIgaG9tZTIuaHRtbFwiLGFzeW5jIGZ1bmN0aW9uKGRvbmUpe1xyXG4gICAgICAgIHRyeXtcclxuICAgICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBmLnJlc29sdmVUZW1wbGF0ZUZpbGUocGF0aC5yZXNvbHZlKF9fZGlybmFtZSwnaG9tZTIuaHRtbCcpKTtcclxuICAgICAgICAgIGRlYnVnZ2VyO1xyXG4gICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0LnRvU3RyaW5nKCkpO1xyXG4gICAgICAgICAgZGVidWdnZXI7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQoe3RpdGxlOlwiRmxhbWVUZW1wbGF0ZVwifSkpO1xyXG4gICAgICAgICAgZG9uZSgpXHJcbiAgICAgICAgfWNhdGNoKGUpe1xyXG4gICAgICAgICAgZG9uZShlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcblxyXG5cclxuICAgIH0pXHJcbiAgfSlcclxufSlcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
