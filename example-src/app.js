import FlameParser from "../src";
var express = require("express");
var path = require("path");

var app = express();

app.use("/", async function(req,res) {
  try {
    var parser = new FlameParser()
    var template = await parser.renderTemplateFile(path.resolve(__dirname, "app.html"));
    console.log(template);
    res.send(template({
      name: "Shavauhn Gabay",
      project: {
        name: "FlamePlatform",
        description: "A Framework Design for code at a glance. Elegance in simplisity",
        create: "December 2015"
      },
      favouriteColors: ["red","green","blue"]
    }))
  } catch (e) {
    console.log(e);
  }
})



app.listen(8080);
