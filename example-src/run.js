'use strict';
import FlameParser from "../src";
var path = require("path");

(async function(req,res) {
  try {
    var parser = new FlameParser()
    var template = await parser.renderTemplateFile(path.resolve(__dirname, "app.html"));
    console.log(template.toString());
    debugger;
    var result = template({
      name: "Shavauhn Gabay",
      project: {
        name: "FlamePlatform",
        description: "A Framework Design for code at a glance. Elegance in simplisity",
        create: "December 2015"
      },
      favouriteColors: "red,green,blue".split(",")
    })
    console.log(result);
  } catch (e) {
    console.log(e);
  }
})()
