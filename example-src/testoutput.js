function anonymous(root
  /**/
) {
  var tmpl = '';
  tmpl += "<html class=\"uk-height-1-1\">";
  tmpl += "\n";
  tmpl += "<head>";
  tmpl += "\n  ";
  tmpl += "<title>";
  tmpl += "" + (root.title || '') + "";
  tmpl += "</title>";
  tmpl += "\n  ";
  tmpl += "<link rel=\"stylesheet\" href=\"/css/style.css\"/>";
  tmpl += "\n  ";
  tmpl += "<script src=\"/js/app.js\">";
  tmpl += "</script>";
  tmpl += "\n";
  tmpl += "</head>";
  tmpl += "\n\n";
  tmpl += "<body class=\"uk-height-1-1 uk-margin-left uk-margin-right\">";
  tmpl += "\n  ";
  tmpl += "\n";
  tmpl += "<h1>";
  tmpl += "" + (root.title || '') + "";
  tmpl += "</h1>";
  tmpl += "\n";
  tmpl += "\n";
  tmpl += "<div class=\"uk-grid uk-margin\">";
  tmpl += "\n  ";
  tmpl += "<div id=\"side-panel\" class=\"uk-width-1-4 uk-height-1-1\">";
  tmpl += "\n    ";
  tmpl += "<div>";
  tmpl += "\n      ";
  tmpl += "<ul class=\"uk-nav uk-margin uk-nav-side\" >";
  for (var menu in root.sidebar.menu) {
    var root1 = {
      menu: root.sidebar.menu[menu]
    };
    tmpl += "\n        ";
    tmpl += "<li class=\"" + ((undefined.menu.active ? 'uk-active' : '') || '') + "\">";
    tmpl += "\n          ";
    tmpl += "<a href=\"" + (root1.menu.link || '') + "\" class=\"uk-width-1-1\">";
    tmpl += "\n            ";
    tmpl += "<span class=\"\">";
    tmpl += "\n              " + (root1.menu.name || '') + "\n            ";
    tmpl += "</span>";
    tmpl += "\n          ";
    tmpl += "</a>";
    tmpl += "\n        ";
    tmpl += "</li>";
    tmpl += "\n      ";
  }
  tmpl += "</ul>";
  tmpl += "\n    ";
  tmpl += "</div>";
  tmpl += "\n\n  ";
  tmpl += "</div>";
  tmpl += "\n  ";
  tmpl += "<div class=\"uk-width-3-4\">";
  tmpl += "\n    ";
  tmpl += "\n";
  tmpl += "<div>";
  tmpl += "Dash";
  tmpl += "</div>";
  tmpl += "\n  ";
  tmpl += "</div>";
  tmpl += "\n";
  tmpl += "</div>";
  tmpl += "\n";
  tmpl += "</body>";
  tmpl += "\n";
  tmpl += "</html>";;
  return tmpl;
}
