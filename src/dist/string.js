import regex from "./regex";

/**
This class is used to parse FlameTemplate Syntax into HTML
**/
export default class StringElement {
  /**
  * @param {String} str String value the lexer produces. This represents one entity.
  *
  */
  constructor(str) {
    this.setText(str);
  }

  /**
  * @param {String} text Sets the string from the lexer. This can perform operations on the string to get it to a more suitable stage.
  */
  setText(text){
    this.text= text.trim().replace(/\n/g,"\\n");
  }


  /**
  * Sets the parent of the string element. This will be another string element
  * @param {StringElement} parent
  *
  *
  */
  setParent(parent) {
    this.parent = parent;
  }

  /**
  *
  *
  *
  *@return {StringElement} parent Returns the parent
  */
  getParent() {
    return this.parent;
  }

  isElement() {
    return false
  }
  isString() {
    return true
  }

  isLoopClose(stack, _key) {
    let element = this;
    if (stack.length === 0) return false;
    let compare = element.text.toLowerCase();
    let [key, test] = stack[stack.length - 1];
    _key = _key || "for";
    if (key === _key) {
      test = test.toLowerCase()
    } else {
      return false;
    }
    return compare.indexOf(test) > -1;
  }

  isIfClose(stack) {
    return this.isLoopClose(stack, 'if');
  }

  static expression(temp,root) {
    let part;
    let r = /\$expression{([^}\n]*)}/;
    while (part = r.exec(temp)) {
      temp = temp.replace(part[0], `"+((${part[1].replace(regex.variable,`${root}.$1`).replace(/\\"/g,"\"")}) ||'')+"`);
    }
    return temp;
  }

  selfpush(){
    this.push(this.text);
  }

  push(str){
    this.temp=this.temp||[];
    this.temp.push(str);
  }

  code() {
    var root = this.root||"root";
    if (this.temp) {
      this.temp = this.temp.map(temp => {
        var result = regex.variable.exec(temp)

        //Expressions
        //$expression{}
        try {

          temp = StringElement.expression(temp,root);
          temp = temp.replace(regex.variable, `"+(${root}.$1 || '')+"`);
        } catch (e) {
          console.log(temp);
          throw e;
        } finally {

        }
        //Statements
        return temp;
      })
    }
  }
}
