import regex from "./regex";
import StringElement from './string';

/**
*Represents HTML tags as well as FlameTemplate Tags
*@extends {StringElement}
*
*
*
*/

export default class Element extends StringElement {

  /**
  *
  *
  *@param {String} str The string passed from the lexer. Represents one html token
  *
  */
  constructor(str) {
    super(str);
    this.children=[];
    this.includes=[];
    this.parent;
  }

  /**
  *This method will allow a child to be added to the element. So if you have a &lt;ul&gt;, underneath the tag can have many children, such as a li
  *
  *@param {StringElement} child Adds a child as to an element.
  *
  */
  addChild(child){
    this.children=this.children||[];
    this.children.push(child);
    child.setParent(this);
  }


  /**
  * Open close tags are tags that are immediately closed.
  * @example <caption>Open Close Tag example</caption>
  * var e = new Element("<input type='button' value='submit'/>");
  * e.isOpenClose() // true
  * @return {Boolean} Checks if a tag is of type openclose
  *
  */
  isOpenClose(){
    return this.isDoctype() || regex.openclose.test(this.text);
  }



  /**
  * Close tags are tags that are closed.
  * @example <caption>Close Tag example</caption>
  * var e = new Element("</div>");
  * e.isClose() // true
  * @return {Boolean} Checks if a tag is of type close
  *
  */
  isClose(){
    return regex.close.test(this.text)
  }


  /**
  * Open tags are tags that are open.
  * @example <caption>Open Tag example</caption>
  * var e = new Element("<div>");
  * e.isOpen() // true
  * @return {Boolean} Checks if a tag is of type open
  *
  */
  isOpen(){
    return !this.isClose() && !this.isDoctype();
  }



  /**
  * Empty tags are tags that contains no information.
  * @example <caption>Empty Tags example</caption>
  * var e = new Element("</>");
  * e.isEmptyElement() // true
  * @return {Boolean} Checks if a tag is of type empty
  *
  */

  isEmptyElement() {
    return regex.empty.test(this.text);
  }


  /**
  * If tags are tags that are if.
  * @example <caption>If Tag example</caption>
  * var e = new Element("<$if>");
  * e.isIf() // true
  * @return {Boolean} Checks if a tag is of type if
  *
  */
  isIf() {
    return regex.if.test(this.text)
  }

  /**
  * Grabs the RegExp result.
  * @example <caption>Get If example</caption>
  * var e = new Element("<$if food>");
  * e.getIf()[1] // food
  * @return {RegExp} Gets the If portion from the tag.
  *
  */
  getIf() {
    let r = regex.if.get.exec(this.text);
    return r;
  }

  getIfElement() {
    let r = this.getIf()[0]
    let e = this.text.replace(r, "");
    if (regex.empty.test(e)) {
      return ""
    } else {
      return e;
    }

  }

  isLoop() {
    return regex.loop.test(this.text)
  }

  getLoop() {
    let r = regex.loop.get.exec(this.text);
    return r;
  }

  getLoopElement() {
    let replace = this.getLoop()[0];
    return this.text.replace(replace, "");
  }

  getLoopElementType() {
    var type = regex.e.tag.exec(this.getLoopElement())[1]
    if (regex.empty.test(type)) {
      return null;
    } else {
      return type;
    }
  }

  isDoctype(){
    return regex.doctype.test(this.text);
  }

  getIfElementType() {
    var type = regex.e.tag.exec(this.getIfElement());
    if (type === null) {
      return null;
    }
    type = type[1];
    if (regex.empty.test(type)) {
      return null;
    } else {
      return type;
    }
  }

  isElse() {
    return regex.else.test(this.text)
  }

  isElseIf() {
    return regex.elseif.test(this.text);
  }

  getElseIf() {
    return regex.elseif.get.exec(this.text)[1]
  }

  getElseIfElement(){
    return this.text.replace(this.getElseIf(),'');
  }

  isElement() {
    return true
  }
  isString() {
    return false
  }

  isClosingTag() {
    return regex.closing.test(this.text);
  }

  isInclude() {
    return regex.include.test(this.text);
  }

  getInclude() {
    return regex.include.get.exec(this.text)[1];
  }

  isParent() {
    return regex.parent.test(this.text);
  }

  getParent() {
    return regex.parent.get.exec(this.text)[1];
  }

  isChild() {
    return regex.child.test(this.text);
  }

  getChild() {
    return regex.child.get.exec(this.text)[0];
  }

  getChildMatch() {
    return regex.child.get.exec(this.text)[0];
  }




}
