const log = require('debug')("flame:template:manager");
let path;
try{
  path = require('path').resolve;
}catch(e){
  path = require("url-join");
}
import FlameParser from './index';

export default class FlameManager{
  /**
  *
  * basedir,directory,dir,d all do the same thing. This is where templates will be searched.
  * You can only have one directory set. Setting multiple directories will only result in one being choosen.
  *
  * @param {{basedir:String,directory:String,dir:String,d:String,cache:Boolean}}
  *
  *
  *
  ****/
  constructor({basedir,directory,dir,d,cache}){

    /**
    * Path to lookup folder
    * @type {String}
    ****/
    this.directory = basedir||directory||dir||d|| process.cwd();

    /**
    * Generated Templates will be stored here
    * @type {Function[]}
    *
    *
    ************/
    this.templates= [];

    /**
    * Whether to cache rendered templates or not
    * @type {Boolean}
    * @experimental This will eventually allow a user to cache or rerender template
    *
    *
    */
    this.cache=cache;
    log(`created templates ${!!this.templates}`);
  }

  /**
  * Produces a function that can be used and reused to generate the HTML page
  *
  * @param {String} path The path to the template file
  * @return {Function} Function template used to render page
  *
  */
  async compile(_path='./'){
    log(`compiling with template cache:${!!this.templates}`)
    if(this.templates[_path] && this.cache){
      return this.templates[_path];
    }

    var location = path(this.directory,_path);
    var f = new FlameParser();
    this.templates[location] = await f.resolveTemplateFile(location);
    return this.templates[location];
  }

  /**
  * Used to compile and Render a template
  * @param {String} path Path to the template file. Either full path or relative path
  * @param {Object} data The data to be used in the template
  * @return {String} The compiled template
  *
  */
  async render(_path,data={}){
      let template = await this.compile(_path);
      return template(data);
  }
}
