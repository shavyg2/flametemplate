const regex={};
regex.if = /[^$]?\$if/
regex.loop = /[^$]?\$for/
regex.else = /[^$]?\$else/
regex.empty = /< *\/? *>/
regex.closing = /< *\//
regex.if.get = /\$if *?([^>/ ]+)/
regex.include = /< *\$include (?:"|')[^"']+(?:"|') *\/ *>/
regex.include.get = /< *\$include (?:"|')([^"']+)(?:"|') *\/ *>/
regex.parent = /^\s*< *\$parent (?:"|')[^"']+(?:"|') *\/ *>/
regex.parent.get = /^\s*< *\$parent (?:"|')([^"']+)(?:"|') *\/ *>/
regex.child = /< *\$child *\/ *>/
regex.child.get = /< *\$child *\/ *>/
regex.loop.get = /\$for *([^ ]+) +of +([^ >]+)/
regex.e = {};
regex.e.tag = /<([A-z]+)/
regex.elseif = /[^$]?\$else *if *[^>]+/
regex.elseif.get = /\$else *if *([^>/]+)/
regex.close = /<(?: *\/ *[^>]*|[^>]+ *\/ *)>/
regex.openclose=/\/ *>$/
regex.variable=/\$([a-z0-9_.]+)/ig
regex.doctype=/< *! *doctype[^>]*>/i;

export default regex;
