import string from './string';
import element from './element';
var log =require("debug")("flame:template:parser");

export default class NodeParser {
  constructor() {
    this.position = 0;
    this.stash = "";
    this.textStack = [];
  }

  parse(string) {
    this.templatestring = string;
    this.length = string.length;
    do {
      this.getToken();
    } while (this.hasNext())
    log(`parsed out ${this.textStack.length} elements`)
    return this.textStack;
  }

  getStructure(string) {

    var stack = this.parse(string);
    var structure = [];
    let structstack = NodeParser.PopulateStructure(structure,stack);
    return structstack;
  }

  static PopulateStructure(arr, stack) {

    while (stack.length !== 0) {
      let node = stack[0];
      if (node.isElement()) {
        log(`found an element ${node.text}`);
        switch (node.isOpenClose() && !node.isEmptyElement() || node.isElse() || node.isElseIf() ? 'openclose' : node.isOpen() ? 'open' : 'close') {
          case 'openclose':
          log(`found open/close tag`);
            var next = stack.shift();
            arr.push(next);
            break;
          case 'open':
            log(`found open tag`)
            var next = stack.shift()
            if(next)
            arr.push(next);
            NodeParser.PopulateStructure(next.children, stack);
            next = stack.shift();
            if(next)
            arr.push(next);
            break;
          case 'close':
            log(`close tag`);
            return arr;
            break;
        }
      } else {
        if(stack.length>0)
        arr.push(stack.shift());
      }
    }
    return arr;
  }

  getToken() {
    let str = this.getNextLetter();
    if (str === "<") {
      this.flush();
      this.stash = str;
    } else if (str === ">") {
      this.stash += str;
      this.flushElement();
      this.stash = "";
    } else {
      this.stash += str;
    }
  }

  hasNext() {
    return this.position <= this.length;
  }


  getNextLetter() {
    return this.templatestring[this.position++];
  }

  flush() {
    if (this.stash.trim() === "") {
      return;
    }
    let str = new string(this.stash);
    this.textStack.push(str);
  }

  flushElement() {
    let str = new element(this.stash);
    this.textStack.push(str);
  }
}
