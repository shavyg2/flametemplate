var wait = require("waitable");
var fs = require("fs");
var debug = require("debug");
var logfor = debug("flame:template:for");
var log = debug("flame:template");
var path = require("path");

import string from './string';

import regex from "./regex";
import element from './element';

import NodeParser from "./nodeparser";


export default class FlameParser2 extends NodeParser {
  constructor() {
    super()
    this.level = 0;
  }

  async renderTemplateFile(file) {

    this.dirname = path.dirname(file);
    this.file = file;
    let content = await wait(fs.readFile, file, {
      encoding: 'utf8'
    });
    let structure = await this.renderTemplate(content);
    return structure;
  }

  async renderTemplate(content) {
    let string = content.replace(/\r/g, '').replace(/"/g, '\\"');
    var structure = this.getStructure(string);
    let structureRendered = await this.renderStructureLoop(structure);
    return structureRendered;
  }


  async resolveTemplate(string){
    let structure = await this.renderTemplate(string);
    let str = await FlameParser2.concatStructure(structure,"");
    try{
      return FlameParser2.makeFunction(str);
    }catch(e){
      console.log(str);
      throw e;
    }
  }

  async resolveTemplateFile(file) {
    let structure = await this.renderTemplateFile(file);
    let str = await FlameParser2.concatStructure(structure, "");
    try {
      return FlameParser2.makeFunction(str);

    } catch (e) {
      console.log(str);
      throw e;
    } finally {

    }
  }

  static makeFunction(str) {
    return new Function('root', `var tmpl='';${str};return tmpl;`);
  }

  static async concatStructure(arr, str) {
    let parent;
    if (arr && arr.length > 0)
      for (var a of arr) {
        if (a.include) {
          let content = await a.include;
          a.children = content;
        }
        if (a.parent) {
          parent = a;
        }
        if (a.temp && a.temp.length > 0) {
          a.code();
          for (var b of a.temp) {
            str += b;
          }
        }
        if (a.children && a.children.length > 0) {
          str = await FlameParser2.concatStructure(a.children, str);
        }
      }
    if (parent) {

      let structure = await parent.parent
      FlameParser2.findChild(structure, str);
      return FlameParser2.concatStructure(structure, "");
    }
    return str;
  }

  static findChild(structure, str) {
    for (let s of structure || []) {
      if (s.child) {

        s.temp = s.temp || [];
        s.temp.push(str);
      } else {
        FlameParser2.findChild(s.children, str);
      }
    }
  }

  async renderStructureLoop(structure) {

    let r = [`root${this.level?this.level:''}`];
    let root = r[r.length - 1];
    let prev=null;
    for (var i = 0, len = structure.length; i < len; i++) {
      var struct = structure[i];
      prev = i > 0 && structure[i-1].isElement() ? structure[i - 1] : prev;

      struct.temp = struct.temp || [];
      struct.root = root;
      let f;

      if (struct.isElement()) {

        //TODO fix for loop end
        let type = struct.isOpenClose() && !struct.isEmptyElement() ? 'openclose' : struct.isOpen() ? 'open' : 'close';
        log(type);


        switch (type) {
          case "openclose":
            type = struct.isInclude() ? 'include' : struct.isParent() ? 'parent' : struct.isChild() ? 'child' : struct.isElseIf() || struct.isElse() ? 'if' : '';

            switch (type) {

              case 'include':
                f = new FlameParser2();
                var _path = path.resolve(this.dirname || "", struct.getInclude());
                struct.include = f.renderTemplateFile(_path);
                break;
              case 'parent':
                f = new FlameParser2();
                var _path = path.resolve(this.dirname || "", struct.getParent());
                if (!struct.parent && !this.parent) {
                  struct.parent = f.renderTemplateFile(_path);
                  this.parent = true;
                }
                break;
              case "if":
                if (struct.isElseIf()) {
                  let statement = struct.getElseIf();
                  let result = /!*/.exec(statement);
                  let neg = "";
                  if (result && result[0]) {
                    statement = statement.replace(/!+/, '');
                    neg = result[0];
                  }
                  struct.temp.push(`}else if(${neg}${root}.${statement}){`)
                } else if (struct.isElse()) {
                  struct.temp.push("}else{");
                } else
                  struct.temp.push(`tmpl+="}"`)
                break;
              case 'child':
                struct.child = true;
                break;
              default:
                struct.temp.push(`tmpl+="${struct.text}";`);
            }
            break;
          case "open":
            type = struct.isIf() ? "if" : struct.isLoop() ? 'for' : 'nothing'
            switch (type) {
              case "if":
                struct.temp.push(`if(${root}.${struct.getIf()[1]}){`);
                struct.temp.push(`tmpl+="${struct.getIfElement()}";`)
                this.renderStructureLoop(struct.children);
                break;
              case "for":
                let loop = struct.getLoop();
                r.push(`root${++this.level}`);
                struct.temp.push(`tmpl+="${struct.getLoopElement()}";`);
                struct.temp.push(`for(var ${loop[1]} in ${root}.${loop[2]}){
                var ${r[r.length-1]}={${loop[1]}:${r[r.length-2]}.${loop[2]}[${loop[1]}]};\n`)
                this.renderStructureLoop(struct.children);
                r.pop();
                break;
              case 'nothing':
              default:
                struct.temp.push(`tmpl+="${struct.text}";`);
                this.renderStructureLoop(struct.children);
            }
            break;
          case "close":
            let _type = type;
            type = prev.isIf() ? "if" : prev.isLoop() ? 'for' : 'nothing'
            switch (type) {
              case 'if':
                struct.temp.push(`tmpl+="${struct.text}";`);
                struct.temp.push("}");
                break;
              case 'for':
                struct.temp.push(`}\ntmpl+="${struct.text}";`)
                this.level--;
                break;
              default:

                struct.temp.push(`tmpl+="${struct.text}";`);

            }
            break;
        }
      } else {
        struct.temp.push(`tmpl+="${struct.text}";`);
      }
    }
    return structure;
  }
}
