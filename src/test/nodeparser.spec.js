import NodeParser from '../dist/nodeparser';
var should = require("chai").should();
import path from "path";
import element from '../dist/element';
import FlameParser2 from "../dist";

describe('NodeParser', function() {

  describe('element', function() {

    it(`should be open`, function() {
      var e = new element("<div>");
      e.isOpen().should.equal(true);
    })

    it(`should be close`, function() {
      var e = new element("</div>");
      e.isClose().should.equal(true);
    })

    it(`should be openclose`, function() {
      var e = new element("<link href='some.css' />");
      e.isOpenClose().should.equal(true);
    })
  })

  describe('Structure', function() {
    let parser;
    beforeEach(function() {
      parser = new NodeParser();
    })
    it('should be able to get unstructured layout of template', function() {
      let str = `<div></div>`;
      var structure = parser.parse(str);
      structure.length.should.equal(2);
    })

    it('should be able to get structured layout of template', function() {
      let str = `<div></div>`;
      var structure = parser.getStructure(str);
      structure.length.should.equal(2);
    })

    it('should be able to get structured layout of template', function() {
      let str = `<div><div>Inner Div</div></div>`;
      var structure = parser.parse(str);
      structure.length.should.equal(5);
    })

    it('should be able to get structured layout of template', function() {
      let str = `<div><div>Inner Div</div></div>`;
      var structure = parser.getStructure(str);

      structure.length.should.equal(2);
      structure[0].children.length.should.equal(2);
      structure[0].children[0].children[0].text.should.equal("Inner Div");
    })
  })


  describe("FlameParser2", function() {
    let f;
    beforeEach(function() {
      f = new FlameParser2();
    })

    it("should be able to parse if", function() {
      let e = new element('<$if !hungry/>');
      e.getIf()[1].should.equal("!hungry");
    })

    it("should be able to find an else if", function() {
      let e = new element('<$else if !hungry/>');
      e.isElseIf().should.equal(true);
    })


    it("should be able to find open tag", function() {
      let e = new element('<$if pizza>');
      e.isOpen().should.equal(true);
      e = new element('<li>')
      e.isOpen().should.equal(true);
    })

    it("should be able to find an if tag", function() {
      let e = new element('<$if pizza>');
      e.isIf().should.equal(true);
    })

    it("should be able to find close tag", function() {
      let e = new element('</>');
      e.isClose().should.equal(true);
      e = new element('</li>')
      e.isClose().should.equal(true);
    })

    it("should be able to render for loop", function() {
      let result = f.renderTemplate(`
        <ul $for name of names>
        <$if pizza>
          pizza
        </>
        <li>$name</li>
        </ul>`)
      let str = "";

      var concat = function(arr, str) {
        if (arr && arr.length > 0)
          for (var a of arr) {
            if (a.temp && a.temp.length > 0)
              for (var b of a.temp) {
                str += b;
              }
            if (a.children && a.children.length > 0) {
              str = concat(a.children, str);
            }
          }
        return str;
      }
      str = concat(result, "");
    })

    describe("File Parsing", function() {
      let f;
      beforeEach(function() {
        f = new FlameParser2()
      })

      it("should be able to parser simple include file", async function(done) {
        let _e;
        try {
          let result = await f.renderTemplateFile(path.resolve(__dirname, 'simple.html'));
        } catch (e) {
          _e = e;
        } finally {
          done(_e)
        }
      })

      it("should be able to parser simple include file", async function(done) {
        let _e;
        try {
          let result = await f.renderTemplateFile(path.resolve(__dirname, 'simpleInclude.html'));
        } catch (e) {
          _e = e;
        } finally {
          done(_e)
        }
      })

      it("should be able to parser complex template file", async function(done) {
        let _e;
        try {
          let result = await f.renderTemplateFile(path.resolve(__dirname, 'template.html'));
        } catch (e) {
          console.log(e.stack);
          _e = e;
        } finally {
          done(_e)
        }
      })

      let testStr;
      it("should be able to parse parent file", async function(done) {
        try {
          let result = await f.resolveTemplateFile(path.resolve(__dirname, 'child.html'));
          testStr = (result());
          done();
        } catch (e) {
          done(e)
        }
      })

      it("should be able to parse parent file with variables", async function(done) {
        try {
          let result = await f.resolveTemplateFile(path.resolve(__dirname, 'childparse.html'));
            (result({
              title: "My Title",
              hello: "hello world"
            })).should.equal(testStr);
          done();
        } catch (e) {
          done(e)
        }
      })


      it("should be able to parse for loop with variables", async function(done) {
        try {
          let result = await f.resolveTemplateFile(path.resolve(__dirname, 'forloop.html'));
          (result({
            title: "My Title",
            menu: ["menu1", "menu2"]
          }));
          done();
        } catch (e) {
          done(e)
        }
      })

      it("should be able to parser home2.html",async function(done){
        try{
          let result = await f.resolveTemplateFile(path.resolve(__dirname,'home2.html'));
          debugger;
          console.log(result.toString());
          debugger;
          console.log(result({title:"FlameTemplate"}));
          done()
        }catch(e){
          done(e);
        }
      })


    })
  })
})
