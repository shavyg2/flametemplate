var sinon = require("sinon");
var should = require("chai").should();
var path = require("path");
import FlameParser from "../dist";
var log = require("debug")("flame:test")

describe("FlameParser", function() {
  let f;
  beforeEach(function() {
    f = new FlameParser();
  })


  it("should be able to parse an element", async function(done) {
    try {
      let result = f.parse("<div>Simple div</div>");
      result[0].isElement().should.equal(true);
      result[1].isString().should.equal(true);
      result[1].text.should.equal("Simple div")
      result[2].isElement().should.equal(true);
      done();
    } catch (e) {
      done(e)
    }
  })

  it("should be able to parser an element with a class", function() {
    let result = f.parse("<div class=\"my-class\"></div>");
    result.length.should.equal(2, "Not all elements were parsed");
    result[0].text.should.equal('<div class="my-class">');
    result[1].text.should.equal('</div>');
  })


  it("should be able to parse a looped element", async function(done) {
    try {
      let result = f.parse("<div $for name of names><div>$name</div></div>");
      result[0].isElement().should.equal(true);
      result[0].isLoop().should.equal(true);
      result[0].getLoopElement().should.equal("<div >");
      result[0].getLoopElementType().should.equal("div")
      let r = result[0].getLoop();
      done();
    } catch (e) {
      done(e)
    }
  })



  it("should be able to parse an element with empty template elements", async function(done) {
    try {
      let result = f.parse("<$if hungry>pizza<$else if !hungry/>no food</>");
      result[0].isIf().should.equal(true);
      let r = result[0].getIf();

      result[2].isElse().should.equal(true);
      r = result[2].getElseIf();

      result[4].isEmptyElement().should.equal(true);
      done();
    } catch (e) {
      done(e)
    }
  })

  it("should be able to parse an element with empty template elements", async function(done) {
    try {
      let result = await f.renderTemplate("<$if hungry>pizza<$else if !hungry/>no food</>");
      done();
    } catch (e) {
      done(e)
    }
  })

  it("should be able to parse an element with empty template elements", async function(done) {
    try {
      let result = await f.resolveTemplate(`

<div $if hungry>
   <h1>my belly</h1>
</div>
<$if hungry>
pizza
<$else if !hungry/>
no food
</>
<ul $for name of names>
<li $for letter of name>
$letter
</li>
</ul>
`);
      result({
        hungry: true,
        names: ["jon", "matt"]
      }).should.equal(`<div ><h1>my belly</h1></div>pizza<ul ><li >jon</li><li >matt</li></ul>`);
      done();
    } catch (e) {
      done(e)
    }
  })

  it("should be able to parse a template file", async function(done) {
    try {
      let result = await f.resolveTemplateFile(path.resolve(__dirname, "template.html"));

      var template = result({
        hungry: true,
        names: ["jon", "matt"]
      });
      template.should.equal(`<div ><h1>my belly</h1></div>pizza<ul ><li >jon</li><li >matt</li></ul>`);
      done();
    } catch (e) {
      done(e)
    }
  })


  it("should be able to detect include elements", async function(done) {
    let e;
    try {
      f.dir = __dirname;
      log(f.dir);
      let result = f.parse(`<div><$include './template.html' /></div>`);
      result[1].isElement().should.equal(true);
      result[1].isInclude().should.equal(true);
    } catch (_e) {
      e = _e;
    } finally {
      done(e);
    }
  })


  it("should be able to detect include elements from string", async function(done) {
    let e;
    try {
      f.dirname = __dirname;
      let result = await f.resolveTemplate(`<div><$include './template.html' /></div>`);
    } catch (_e) {
      e = _e;
    } finally {
      done(e);
    }
  })

  it("should be able to detect include elements from file", async function(done) {
    let e;
    try {
      let result = await f.renderTemplateFile(path.resolve(__dirname,'home.html'));
    } catch (_e) {
      e = _e;
    } finally {
      done(e);
    }
  })

  it("should be able to render include elements", async function(done) {
    let e;
    try {
      let result = await f.resolveTemplateFile(path.resolve(__dirname,'simpleInclude.html'));
      result({text:"This is test"}).should.equal("<div><div>This is test</div></div>");
    } catch (_e) {
      e = _e;
    } finally {
      done(e);
    }
  })

  it('should be able to find child element',async function(done){
    let e;
    try {
      let result = f.parse("<div><$child/></div>");
      result[1].isChild().should.equal(true);
      result[1].getChild().should.equal('<$child/>');
    } catch (_e) {
      e=_e
    } finally {
      done(e)
    }
  })

  it('should be able to find parent element',async function(done){
    let e;
    try {
      let result = f.parse("<$parent './parent.html'/><div>my block</div>");
      result[0].isParent().should.equal(true);
      result[0].getParent().should.equal('./parent.html');
    } catch (_e) {
      e=_e
    } finally {
      done(e)
    }
  })



  it('should be able to render a child template',async function(done){
    let e;
    try {
      let result = await f.resolveTemplateFile(path.resolve(__dirname,'child.html'));
      should.exist(result);
      (typeof result).should.equal('function');

    } catch (_e) {
      e=_e
    } finally {
      done(e)
    }
  })


})
