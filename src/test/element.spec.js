import chai from "chai";
const should = chai.should();
import string from "../dist/string";
import Element from "../dist/element";


describe("An Element",function(){

  describe("Class",function(){

  })


  describe("Methods",function(){
      let e;
      before(function(){
        e = new Element("<div class='$person'>$person $detail</div>");
      })

      it("selfpush should create temp property",function(){
        should.not.exist(e.temp);
        e.selfpush();
        should.exist(e.temp);
      })

      it("code should swap the temp object",function(){
        var _1 = e.temp;
        e.code();
        var _2 = e.temp;
        _1.should.not.equal(_2);
      })

      it("code should be parsed",function(){
        e.temp[0].should.equal(`<div class='"+(root.person || '')+"'>"+(root.person || '')+" "+(root.detail || '')+"</div>`);
      })

  })




})
